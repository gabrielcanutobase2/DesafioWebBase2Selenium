package br.com.serenitybddtemplate.dbsteps;

import br.com.serenitybddtemplate.utils.DBUtils;
import br.com.serenitybddtemplate.utils.GeneralUtils;
import net.serenitybdd.annotations.Step;

import java.util.ArrayList;

public class UsersDBSteps extends DBUtils {

    private String sqlPath = "src/test/java/br/com/serenitybddtemplate/sqls/users/";

    @Step("Valida usuario por nome '{0}'")
    public String validaNomeUsuarioDB(String usuario) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaUserCompletoPorNome.sql");
        query = query.replace("$usuario", usuario);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(1);
    }

    @Step("Retorna Usuario Completo por nome '{0}'")
    public ArrayList<String> retornaUsuarioPorNome(String usuario) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaUserCompletoPorNome.sql");
        query = query.replace("$usuario", usuario);
        return retornaDadosQuery(query);
    }

    @Step("Retorna Usuario por id '{0}'")
    public ArrayList<String> retornaUsuarioPorIdDB(String usuario) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaUserCompletoPorID.sql");
        query = query.replace("$userID", usuario);

        return retornaDadosQuery(query);
    }


    public ArrayList<String> retornaUserCompletoPorNome(String usuario) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaUserCompletoPorNome.sql");
        query = query.replace("$usuario", usuario);

        return retornaDadosQuery(query);
    }

    public int retornaAtividadeUsuarioDB(String usuario) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaUserCompletoPorNome.sql");
        query = query.replace("$usuario", usuario);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return Integer.parseInt(resultado.get(5));
    }

    public void MassaUsuarios() {

        String queryInsereUsuarioAdmin = GeneralUtils.getFileContentAsString(sqlPath + "insereUsuarioAdmin.sql");
        String queryAtualizaUsuarioAdmin = GeneralUtils.getFileContentAsString(sqlPath + "atualizaUsuarioAdmin.sql");
        if (retornaUserCompletoPorNome("administrator") == null) {

            String usuario = queryInsereUsuarioAdmin.replace("$usuario", "administrator").replace("$nome", "").replace("$email", "root@localhost").replace("$cookie", "9WSDEYfLh6UmuaVmoHSqM9_EObaOxy9765ijMbNauV430IiTRy49MpN847X7OE9Q");
            executaMudancas(usuario);
        } else {
            if (!retornaUserCompletoPorNome("administrator").get(4).equals("3e233dfa861b2fe52cefbed0e2200c4a")) {
                String usuario = queryAtualizaUsuarioAdmin.replace("$usuario", "administrator");
                executaMudancas(usuario);
            }
        }
        if (retornaUserCompletoPorNome("user1") == null) {
            String usuario = queryInsereUsuarioAdmin.replace("$usuario", "user1").replace("$nome", "Nome Usuario 1 Teste").replace("$email", "user1.teste@email.com").replace("$cookie", "5a269a3533d24c6b969c11606a1490ce");
            executaMudancas(usuario);
        }
        if (retornaUserCompletoPorNome("user2") == null) {
            String usuario = queryInsereUsuarioAdmin.replace("$usuario", "user2").replace("$nome", "Nome Usuario 2 Teste").replace("$email", "user2.teste@email.com").replace("$cookie", "5a269a3533d24c6b969c11606a1490ca");
            executaMudancas(usuario);
        }
        if (retornaUserCompletoPorNome("user3") == null) {
            String usuario = queryInsereUsuarioAdmin.replace("$usuario", "user3").replace("$nome", "Nome Usuario 3 Teste").replace("$email", "user3.teste@email.com").replace("$cookie", "5a269a3533d24c6b969c11606a1460De");
            executaMudancas(usuario);
        }
    }

    public ArrayList<String> VerificaContaBD(String usuario) {
        String queryVerificaConta = GeneralUtils.getFileContentAsString(sqlPath + "retornaUser.sql");
        String conta = queryVerificaConta.replace("$usuario", usuario);
        return retornaDadosQuery(conta);
    }

    public void CarregaUmUsuarioInativo() {
        String queryInsereUsuarioInativo = GeneralUtils.getFileContentAsString(sqlPath + "insereUsuarioDesativado.sql");

        if (VerificaContaBD("usuario.desativado") == null) {
            String usuario = queryInsereUsuarioInativo.replace("$usuario", "usuario.desativado").replace("$nome", "Usuario Desativado").replace("$email", "usuario.desativado@email.com").replace("$cookie", "5a269a3533d24c6b969c11606a1460Re");
            executaMudancas(usuario);
        }

    }

    public void DeleteUsuariosAfterDone() {
        String queryDeletaContaPosUso = GeneralUtils.getFileContentAsString(sqlPath + "deletaUsuario.sql");
        queryDeletaContaPosUso = queryDeletaContaPosUso.replace("$usuario", "administrator");
        executaMudancas(queryDeletaContaPosUso);
    }

}
