package br.com.serenitybddtemplate.dbsteps;

import br.com.serenitybddtemplate.utils.DBUtils;
import br.com.serenitybddtemplate.utils.GeneralUtils;
import net.serenitybdd.annotations.Step;


import java.util.ArrayList;

public class ProjetosDBSteps extends DBUtils {

    private String sqlPath = "src/test/java/br/com/serenitybddtemplate/sqls/projects/";

    @Step("Retorna Nome projeto '{0}'")
    public String retornaNomeDoProjetoDB(String nome) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaProjetoNome.sql");
        query = query.replace("$projeto", nome);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(0);
    }


    public void MassaProjetos() {
        String queryVerificaProjeto = GeneralUtils.getFileContentAsString(sqlPath + "retornaProjetoNome.sql");
        String queryInsereProjeto = GeneralUtils.getFileContentAsString(sqlPath + "insereProjeto.sql");
        String projeto1 = queryVerificaProjeto.replace("$projeto", "projeto1");
        String projeto2 = queryVerificaProjeto.replace("$projeto", "projeto2");
        String projeto3 = queryVerificaProjeto.replace("$projeto", "projeto3");
        ArrayList<String> resultadoProjeto1 = retornaDadosQuery(projeto1);
        ArrayList<String> resultadoProjeto2 = retornaDadosQuery(projeto2);
        ArrayList<String> resultadoProjeto3 = retornaDadosQuery(projeto3);

        if (resultadoProjeto1 == null) {
            String projeto = queryInsereProjeto.replace("$projeto", "Projeto Teste 1").replace("$descricao", "Teste de Descricao 1 ");
            executaMudancas(projeto);
        }
        if (resultadoProjeto2 == null) {
            String projeto = queryInsereProjeto.replace("$projeto", "Projeto Teste 2").replace("$descricao", "Teste de Descricao 1");
            executaMudancas(projeto);
        }
        if (resultadoProjeto3 == null) {
            String projeto = queryInsereProjeto.replace("$projeto", "Projeto Teste 3").replace("$descricao", "Teste de Descricao 1");
            executaMudancas(projeto);
        }
    }

    public void DeleteProjetosAfterDone() {
        String queryDeletaProjetoPosUso = GeneralUtils.getFileContentAsString(sqlPath + "deletaProjeto.sql");
        executaMudancas(queryDeletaProjetoPosUso);
    }

    public void DeleteCategoriasAfterDone() {
        String queryDeletaCategoriaPosUso = GeneralUtils.getFileContentAsString(sqlPath + "deletaCategorias.sql");
        executaMudancas(queryDeletaCategoriaPosUso);
    }

}
