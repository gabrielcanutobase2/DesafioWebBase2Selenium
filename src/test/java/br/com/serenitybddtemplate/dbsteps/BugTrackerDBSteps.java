package br.com.serenitybddtemplate.dbsteps;

import br.com.serenitybddtemplate.utils.DBUtils;
import br.com.serenitybddtemplate.utils.GeneralUtils;
import net.serenitybdd.annotations.Step;


import java.util.ArrayList;

public class BugTrackerDBSteps extends DBUtils {

    private String sqlPath = "src/test/java/br/com/serenitybddtemplate/sqls/bugTracker/";

    @Step("Retorna Resumo Tarefa '{0}'")
    public String retornaResumoDaTarefaDB(String bugTextID) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaResumoTarefaPorBugID.sql");
        query = query.replace("$bugID", bugTextID);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(0);
    }

    @Step("Retorna ID Tarefa '{0}'")
    public String retornaIDoDaTarefaDB(String resumo) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaBugIDTarefaPorBugResumo.sql");
        query = query.replace("$resumo", resumo);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(0);
    }

    @Step("Retorna Marcador por ID'")
    public String retornaMarcadorPorID(String bugID) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaMarcadorPorID.sql");
        query = query.replace("$bugID", bugID);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(0);
    }

    @Step("Retorna Marcador por nome ")
    public ArrayList<String> retornaMarcadorPorNome(String tagName) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaMarcadorPorNome.sql");
        query = query.replace("$tagName", tagName);
        return retornaDadosQuery(query);
    }

    @Step("Retorna Relacionamento Source_Bug_ID")
    public String retornaRelacionamentoSourceBug(String bugID, String destinationBugID) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaRelacionamentoTarefa.sql");
        query = query.replace("$bugID", bugID).replace("$destinationBugID", destinationBugID);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(1);
    }

    public void CarregaMarcadores() {
        String queryMarcadores = GeneralUtils.getFileContentAsString(sqlPath + "insereMarcadoresNoBanco.sql");
        String queryMarcadoresDeTarefas = GeneralUtils.getFileContentAsString(sqlPath + "insereMarcadoresDeTarefasNoBanco.sql");
        String queryRetornaListaMarcadores = GeneralUtils.getFileContentAsString(sqlPath + "retornaListaMarcadores.sql");
        String queryRetornaListaMarcadoresTarefas = GeneralUtils.getFileContentAsString(sqlPath + "retornaListaMarcadoresTarefas.sql");

        ArrayList<String> resultadoMarcadores = retornaDadosQuery(queryRetornaListaMarcadores);
        ArrayList<String> resultadoMarcadoresTarefas = retornaDadosQuery(queryRetornaListaMarcadoresTarefas);

        if (resultadoMarcadores == null && resultadoMarcadoresTarefas == null) {
            executaMudancas(queryMarcadores);
            executaMudancas(queryMarcadoresDeTarefas);
        }
    }

    @Step("Retorna Relacionamento Destination_Bug_ID")
    public String retornaRelacionamentoDestinationBug(String bugID, String destinationBugID) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaRelacionamentoTarefa.sql");
        query = query.replace("$bugID", bugID).replace("$destinationBugID", destinationBugID);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(2);
    }


    @Step("Retorna Relacionamento RelationshipType")
    public String retornaRelacionamentoRelationshipType(String bugID, String destinationBugID) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaRelacionamentoTarefa.sql");
        query = query.replace("$bugID", bugID).replace("$destinationBugID", destinationBugID);
        ArrayList<String> resultado = retornaDadosQuery(query);
        return resultado.get(3);
    }

    @Step("Verifica Se Não tem marcador por ID'")
    public String verificaMarcadorVazioPorID(String bugID) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaMarcadorPorID.sql");
        query = query.replace("$bugID", bugID);
        ArrayList<String> resultado = retornaDadosQuery(query);
        if (resultado == null) {
            return null;
        }
        return "Não foi nulo";
    }

    @Step("Retorna Monitoramento por id de tarefa '{0}'")
    public ArrayList<String> retornaMonitoramentoPorIdDB(String bugID, String userID) {
        String query = GeneralUtils.getFileContentAsString(sqlPath + "retornaMonitoramentoPorIDTarefa.sql");
        query = query.replace("$bugID", bugID).replace("$userID", userID);

        return retornaDadosQuery(query);
    }

    public void DeleteMarcadoresTarefaAfterDone() {
        String verificaExistenciaMarcadores = GeneralUtils.getFileContentAsString(sqlPath + "retornaListaMarcadores.sql");
        ArrayList<String> resultados = retornaDadosQuery(verificaExistenciaMarcadores);
        if (resultados != null) {
            String queryDeletaMarcadoresDeTarefas = GeneralUtils.getFileContentAsString(sqlPath + "deletaMarcadoresDeTarefas.sql");
            String queryDeletaMarcadores = GeneralUtils.getFileContentAsString(sqlPath + "deletaMarcadores.sql");
            executaMudancas(queryDeletaMarcadoresDeTarefas);
            executaMudancas(queryDeletaMarcadores);
        }
    }

    public void DeleteRelacionamentoTarefaAfterDone() {
        String verificaExistenciaRelacionamento = GeneralUtils.getFileContentAsString(sqlPath + "retornaListaRelacionamentoExistente.sql");
        ArrayList<String> resultados = retornaDadosQuery(verificaExistenciaRelacionamento);

        if (resultados != null) {
            String queryDeletaRelacionamento = GeneralUtils.getFileContentAsString(sqlPath + "deletaRelacionamento.sql");
            executaMudancas(queryDeletaRelacionamento);

        }

    }

    public void DeleteTarefasAfterDone() {
        String verificaExistenciaRelacionamento = GeneralUtils.getFileContentAsString(sqlPath + "retornaListaTarefas.sql");
        ArrayList<String> resultados = retornaDadosQuery(verificaExistenciaRelacionamento);

        if (resultados != null) {
            String queryDeletaTarefas = GeneralUtils.getFileContentAsString(sqlPath + "deletaTarefa.sql");
            executaMudancas(queryDeletaTarefas);

        }

    }

}
