package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.dbsteps.BugTrackerDBSteps;
import br.com.serenitybddtemplate.dbsteps.UsersDBSteps;
import br.com.serenitybddtemplate.steps.DashBoardSteps;
import br.com.serenitybddtemplate.steps.VerTarefasSteps;
import br.com.serenitybddtemplate.utils.Constants;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.annotations.Steps;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class VerTarefasStepDefinitions {
    @Steps
    VerTarefasSteps verTarefasSteps;

    @Steps
    DashBoardSteps dashBoardSteps;


    @E("insiro o ID da tarefa no campo de filtro")
    public void insiroOIDDaTarefaNoCampoDePesquisaDeFiltro() throws IOException {
        String tarefaID = Serenity.sessionVariableCalled("bugID");
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisaDeFiltro(tarefaID);

    }

    @E("clico em Aplicar Filtro")
    public void clicoEmAplicarFiltro() throws IOException {
        verTarefasSteps.clicoEmAplicarFiltro();

    }

    @Entao("valido numero de registro da tarefa no quadro")
    public void validoNumeroDeRegistroDaTarefaNoQuadro() throws IOException {
        String tarefaID = Serenity.sessionVariableCalled("bugID");
        Assert.assertTrue(verTarefasSteps.retornaIDTarefaTabela(tarefaID));

    }


    @E("insiro um ID INVALIDO da tarefa no campo de filtro")
    public void insiroUmIDInvalidoDaTarefaNoCampoDePesquisaDeFiltro() throws IOException {
        String tarefaID = "!1235888";
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisaDeFiltro(tarefaID);

    }

    @Entao("valido que não existe numero da tarefa no quadro")
    public void validoQueNaoExiteNumeroDaTarefaNoQuadro() throws IOException {
        Assert.assertTrue(verTarefasSteps.validoQueNaoExisteNumeroDaTarefaNoQuadro());

    }

    @E("insiro o ID da tarefa no campo de pesquisa")
    public void insiroOIDDaTarefaNoCampoDePesquisa() throws IOException {
        String tarefaID = Serenity.sessionVariableCalled("bugID");
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisa(tarefaID);

    }

    @Entao("verifico se a tarefa mostrada na tela tem o mesmo ID da tabela criada")
    public void verificoSeATarefaMostradaNaTelaTemOMesmoIDDaTabelaCriada() throws IOException {
        Assert.assertEquals(Serenity.sessionVariableCalled("bugID"), verTarefasSteps.retornIDTarefaVerDetalhes());

    }

    @E("insiro um ID INVALIDO da tarefa no campo de pesquisa")
    public void insiroUmIDINVALIDODaTarefaNoCampoDePesquisa() throws IOException {
        String tarefaIDInvalida = RandomStringUtils.randomNumeric(3, 5);
        Serenity.setSessionVariable("bugIDInvalido").to(tarefaIDInvalida);
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisa(tarefaIDInvalida);

    }

    @Entao("valido mensagem de erro numero de tarefa não encontrado")
    public void validoMensagemDeErroNumeroDeTarefaNaoEncontrado() throws IOException {
        String mensagemEsperada = String.format(Constants.TAREFA_PESQUISA_ERRO_NAO_ENCONTRADA, Serenity.sessionVariableCalled("bugIDInvalido").toString());
        Assert.assertEquals(mensagemEsperada, verTarefasSteps.retornaMensagemErro());

    }

    @E("insiro um caractere no campo de pesquisa")
    public void insiroUmCaractereNoCampoDePesquisa() throws IOException {
        String caractere = "aaaa";
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisa(caractere);

    }

    @Entao("valido mensagem de erro caracter inserido não esperado")
    public void validoMensagemDeErroCaracterInseridoNãoEsperado() throws IOException {
        Assert.assertEquals(Constants.TAREFA_PESQUISA_ERRO_LETRA_EM_CAMPO_NUMERICO, verTarefasSteps.retornaMensagemErro());

    }

    @E("insiro um hifen no campo de pesquisa")
    public void insiroUmHifenNoCampoDePesquisa() throws IOException {
        String hifen = "-";
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisa(hifen);


    }

    @Entao("valido mensagem de erro tarefa 0 não encontrada")
    public void validoMensagemDeErroTarefaNãoEncontrada() throws IOException {
        Assert.assertEquals(Constants.TAREFA_PESQUISA_ERRO_HIFEN_UNICO_EM_CAMPO_NUMERICO, verTarefasSteps.retornaMensagemErro());

    }

    @E("insiro um símbolo no campo de pesquisa")
    public void insiroUmSimboloNoCampoDePesquisa() throws IOException {
        String hifen = "%$#";
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisa(hifen);

    }

    @Entao("valido mensagem de erro simbolo não esperado")
    public void validoMensagemDeErroSimboloNãoEsperado() throws IOException {
        Assert.assertEquals(Constants.TAREFA_PESQUISA_ERRO_SIMBOLO_EM_CAMPO_NUMERICO, verTarefasSteps.retornaMensagemErro());

    }

    @E("pesquiso pelo ID da tarefa")
    public void pesquisoPeloIDDaTarefa() throws IOException {
        String tarefaID = Serenity.sessionVariableCalled("bugID");
        verTarefasSteps.insiroOIDDaTarefaNoCampoDePesquisa(tarefaID);


    }

    @E("preencho o campo de Marcadores com um marcador")
    public void preenchoOCampoDeMarcadoresComUmMarcador() throws IOException {
        String tag = "Marcador " + RandomStringUtils.randomAlphanumeric(2, 6);
        Serenity.setSessionVariable("tagName").to(tag);
        verTarefasSteps.preenchoOCampoDeMarcadoresComUmMarcador(tag);


    }


    @E("clico em Aplicar Marcador")
    public void clicoEmAplicarMarcador() throws IOException {
        verTarefasSteps.clicoEmAplicarMarcador();

    }

    @Entao("verifico no banco de dados se o marcador existe")
    public void verificoNoBancoDeDadosSeOMarcadorExiste() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String resumo = Serenity.sessionVariableCalled("resumo");
        String bugID = bugTrackerDBSteps.retornaIDoDaTarefaDB(resumo);
        Assert.assertEquals(bugID, bugTrackerDBSteps.retornaMarcadorPorID(bugID));

    }

    @E("Excluo o marcador criado")
    public void excluoOMarcadorCriado() throws IOException {
        verTarefasSteps.excluoOMarcadorCriado();

    }

    @Entao("verifico no banco de dados se o marcador foi excluido com sucesso")
    public void verificoNoBancoDeDadosSeOMarcadorFoiExcluidoComSucesso() {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String resumo = Serenity.sessionVariableCalled("resumo");
        String bugID = bugTrackerDBSteps.retornaIDoDaTarefaDB(resumo);
        Assert.assertNull(bugTrackerDBSteps.verificaMarcadorVazioPorID(bugID));
    }

    @E("seleciono relacao {string}")
    public void selecionoRelacao(String tipoRelacao) throws IOException {
        verTarefasSteps.selecionoRelacao(tipoRelacao);

    }

    @E("preencho campo de relacao com o ID da tarefa a ser relacionada")
    public void preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada() throws IOException {

        String tarefaSecundariaID = Serenity.sessionVariableCalled("bugIDSecundaria");
        verTarefasSteps.preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada(tarefaSecundariaID);

    }

    @E("clico em Adicionar")
    public void clicoEmAdicionar() throws IOException {
        verTarefasSteps.clicoEmAdicionar();

    }

    @Entao("verifico no banco de dados se foi adicionada a relação do tipo 1 a tarefa")
    public void verificoNoBancoDeDadosSeFoiAdicionadaARelaçãoDoTipo1ATarefa() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String task_BugID_Default = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String source_bugID = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String destination_bugID = Serenity.sessionVariableCalled("bugIDSecundaria").toString().replaceAll("^0+(?!$)", "");

        Assert.assertEquals(destination_bugID, bugTrackerDBSteps.retornaRelacionamentoDestinationBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals(source_bugID, bugTrackerDBSteps.retornaRelacionamentoSourceBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals("1", bugTrackerDBSteps.retornaRelacionamentoRelationshipType(task_BugID_Default, destination_bugID));


    }


    @Entao("verifico no banco de dados se foi adicionada a relação do tipo 2 Pai a tarefa")
    public void verificoNoBancoDeDadosSeFoiAdicionadaARelaçãoDoTipo2PaiATarefa() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String task_BugID_Default = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String source_bugID = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String destination_bugID = Serenity.sessionVariableCalled("bugIDSecundaria").toString().replaceAll("^0+(?!$)", "");

        Assert.assertEquals(destination_bugID, bugTrackerDBSteps.retornaRelacionamentoDestinationBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals(source_bugID, bugTrackerDBSteps.retornaRelacionamentoSourceBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals("2", bugTrackerDBSteps.retornaRelacionamentoRelationshipType(task_BugID_Default, destination_bugID));


    }

    @Entao("verifico no banco de dados se foi adicionada a relação do tipo 2 Filho a tarefa")
    public void verificoNoBancoDeDadosSeFoiAdicionadaARelaçãoDoTipo2FilhoATarefa() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String task_BugID_Default = Serenity.sessionVariableCalled("bugIDSecundaria").toString().replaceAll("^0+(?!$)", "");
        String source_bugID = Serenity.sessionVariableCalled("bugIDSecundaria").toString().replaceAll("^0+(?!$)", "");
        String destination_bugID = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");


        Assert.assertEquals(source_bugID, bugTrackerDBSteps.retornaRelacionamentoSourceBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals(destination_bugID, bugTrackerDBSteps.retornaRelacionamentoDestinationBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals("2", bugTrackerDBSteps.retornaRelacionamentoRelationshipType(source_bugID, destination_bugID));


    }

    @Entao("verifico no banco de dados se foi adicionada a relação do tipo 0 a tarefa")
    public void verificoNoBancoDeDadosSeFoiAdicionadaARelaçãoDoTipo0ATarefa() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String task_BugID_Default = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String source_bugID = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String destination_bugID = Serenity.sessionVariableCalled("bugIDSecundaria").toString().replaceAll("^0+(?!$)", "");

        Assert.assertEquals(destination_bugID, bugTrackerDBSteps.retornaRelacionamentoDestinationBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals(source_bugID, bugTrackerDBSteps.retornaRelacionamentoSourceBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals("0", bugTrackerDBSteps.retornaRelacionamentoRelationshipType(task_BugID_Default, destination_bugID));


    }

    @Entao("verifico no banco de dados se foi adicionada a relação do tipo 0 Possui Duplicado a tarefa")
    public void verificoNoBancoDeDadosSeFoiAdicionadaARelaçãoDoTipo0PossuiDuplicadoATarefa() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String task_BugID_Default = Serenity.sessionVariableCalled("bugIDSecundaria").toString().replaceAll("^0+(?!$)", "");
        String source_bugID = Serenity.sessionVariableCalled("bugIDSecundaria").toString().replaceAll("^0+(?!$)", "");
        String destination_bugID = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");

        Assert.assertEquals(destination_bugID, bugTrackerDBSteps.retornaRelacionamentoDestinationBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals(source_bugID, bugTrackerDBSteps.retornaRelacionamentoSourceBug(task_BugID_Default, destination_bugID));
        Assert.assertEquals("0", bugTrackerDBSteps.retornaRelacionamentoRelationshipType(task_BugID_Default, destination_bugID));


    }

    @E("preencho campo de relacao com um ID que é inexistente")
    public void preenchoCampoDeRelacaoComOIDDaTarefaInvalida() throws IOException {
        String tarefaInvalida = "9991234";
        Serenity.setSessionVariable("bugIDInvalido").to(tarefaInvalida);
        verTarefasSteps.preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada(tarefaInvalida);


    }

    @Entao("valido mensagem de erro id da tarefa nao encontrada")
    public void validoMensagemDeErroIdDaTarefaNaoEncontrada() throws IOException {
        String mensagemEsperada = String.format(Constants.TAREFA_RELACIONAR_ERRO_ID_NAO_ENCONTRADO, Serenity.sessionVariableCalled("bugIDInvalido").toString());
        Assert.assertEquals(mensagemEsperada, verTarefasSteps.retornaMensagemErro());


    }

    @E("preencho campo de relacao com letras")
    public void preenchoCampoDeRelacaoComUmValorInvalido() throws IOException {
        String caractere = "aaaa";
        verTarefasSteps.preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada(caractere);


    }

    @Entao("valido mensagem de erro valor invalido")
    public void validoMensagemDeErroValorInvalido() throws IOException {
        Assert.assertEquals(Constants.TAREFA_RELACIONAR_ERRO_LETRA_EM_CAMPO_NUMERICO, verTarefasSteps.retornaMensagemErro());

    }

    @E("preencho campo de relacao com hifens e simbolos")
    public void preenchoCampoDeRelacaoComHifensESimbolos() throws IOException {
        String caractere = "-%#$(";
        verTarefasSteps.preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada(caractere);


    }

    @Entao("valido mensagem de erro valor invalido hifens e simbolos")
    public void validoMensagemDeErroValorInvalidoHifensESimbolos() throws IOException {
        Assert.assertEquals(Constants.TAREFA_RELACIONAR_ERRO_HIFEN_UNICO_EM_CAMPO_NUMERICO, verTarefasSteps.retornaMensagemErro());


    }

    @E("preencho campo de relacao com o ID da tarefa da pagina atual")
    public void preenchoCampoDeRelacaoComOIDDaTarefaDaPaginaAtual() throws IOException {
        String bugID = Serenity.sessionVariableCalled("bugID");
        verTarefasSteps.preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada(bugID);


    }

    @Entao("valido mensagem de erro tarefa não pode relacionar consigo mesmo")
    public void validoMensagemDeErroTarefaNãoPodeRelacionarConsigoMesmo() throws IOException {
        Assert.assertEquals(Constants.TAREFA_RELACIONAR_ERRO_ID_PROPRIO_ID, verTarefasSteps.retornaMensagemErro());

    }

    @E("preencho campo usuários monitorando tarefa  o nome do usuario desejado")
    public void preenchoCampoUsuariosMonitorandoTarefaONomeDoUsuarioDesejado() throws IOException {
        Random random = new Random();
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String userID = usersDBSteps.retornaUsuarioPorIdDB("1").get(0);
        String userName = usersDBSteps.retornaUsuarioPorIdDB("1").get(1);
        Serenity.setSessionVariable("usuario").to(userName);
        Serenity.setSessionVariable("usuarioID").to(userID);
        verTarefasSteps.preenchoCampoUsuariosMonitorandoTarefaONomeDoUsuarioDesejado(userName);


    }

    @E("clico em Adicionar Usuario")
    public void clicoEmAdicionarUsuario() throws IOException {
        verTarefasSteps.clicoEmAdicionarUsuario();


    }

    @Entao("valido no banco que o usuario agora esta monitorando a tarefa")
    public void validoNoBancoQueOUsuarioAgoraEstaMonitorandoATarefa() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String bugID = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String userID = Serenity.sessionVariableCalled("usuarioID");
        String userIdBD = bugTrackerDBSteps.retornaMonitoramentoPorIdDB(bugID, userID).get(0);
        String bugIdBD = bugTrackerDBSteps.retornaMonitoramentoPorIdDB(bugID, userID).get(1);

        Assert.assertEquals(bugID, bugIdBD);
        Assert.assertEquals(userID, userIdBD);


    }

    @E("preencho campo usuários monitorando tarefa  o nome de um usuario invalido")
    public void preenchoCampoUsuáriosMonitorandoTarefaONomeDeUmUsuarioInvalido() throws IOException {
        String userName = "usuarioInvalido123";
        Serenity.setSessionVariable("usuarioInvalido").to(userName);
        verTarefasSteps.preenchoCampoUsuariosMonitorandoTarefaONomeDoUsuarioDesejado(userName);


    }

    @Entao("valido mensagem de erro usuario não encontrado")
    public void validoMensagemDeErroUsuarioNãoEncontrado() throws IOException {
        String usuario = Serenity.sessionVariableCalled("usuarioInvalido");
        String mensagemEsperada = String.format(Constants.TAREFA_MONITORAMENTO_USUARIO_NAO_ENCONTRADO, usuario);
        Assert.assertEquals(mensagemEsperada, verTarefasSteps.retornaMensagemErro());


    }

    @E("excluo o usuario adicionado")
    public void excluoOUsuarioAdicionado() throws IOException {
        verTarefasSteps.clicoExcluoOUsuarioAdicionado();


    }

    @Entao("valido no banco que o usuario não esta mais na tabela")
    public void validoNoBancoQueOUsuarioNãoEstaMaisNaTabela() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String bugID = Serenity.sessionVariableCalled("bugID").toString().replaceAll("^0+(?!$)", "");
        String userID = Serenity.sessionVariableCalled("usuarioID");
        ArrayList<String> userIdBD = bugTrackerDBSteps.retornaMonitoramentoPorIdDB(bugID, userID);

        Assert.assertNull(userIdBD);


    }

    @E("deixo o campo de usuario vazio")
    public void deixoOCampoDeUsuarioVazio() throws IOException {
        verTarefasSteps.preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada("");


    }

    @Entao("valido mensagem de erro campo usuario vazio")
    public void validoMensagemDeErroCampoUsuarioVazio() throws IOException {
        Assert.assertEquals(Constants.TAREFA_RELACIONAR_ERRO_CAMPO_VAZIO, verTarefasSteps.retornaMensagemErro());


    }
}
