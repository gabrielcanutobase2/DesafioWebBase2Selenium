package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.dbsteps.BugTrackerDBSteps;
import br.com.serenitybddtemplate.steps.BugTrackerSteps;
import br.com.serenitybddtemplate.steps.DashBoardSteps;
import br.com.serenitybddtemplate.utils.Constants;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import net.serenitybdd.annotations.Steps;
import net.serenitybdd.core.Serenity;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;

import java.io.IOException;


public class BugTrackerStepDefinitions {
    @Steps

    DashBoardSteps dashBoardSteps;
    @Steps
    BugTrackerSteps bugTrackerSteps;


    @E("preencho os campos obrigatórios do Relatório")
    public void preenchoOsCamposDoRelatórioCom() throws IOException {
        Lorem lorem = LoremIpsum.getInstance();
        String categoria = "[Todos os Projetos] General";
        String resumo = "Teste de Resumo " + RandomStringUtils.randomAlphanumeric(5, 10);
        Serenity.setSessionVariable("resumo").to(resumo);
        String descricao = "Teste de descricao " + lorem.getParagraphs(1, 3);
        bugTrackerSteps.preenchoOsCamposDoRelatórioCom(categoria, resumo, descricao);

    }


    @E("clico em Criar Nova tarefa")
    public void clicoEmCriarNovaTarefa() throws IOException {
        bugTrackerSteps.clicoEmCriarNovaTarefa();

    }

    @Entao("verifico se tarefa foi criada com sucesso pelo banco de dados")
    public void verificoSeTarefaFoiCriadaComSucessoPeloBancoDeDados() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String bugID = bugTrackerSteps.retornaTextoBugID().replaceAll("^0+(?!$)", "");

        Assert.assertEquals(Serenity.sessionVariableCalled("resumo"), bugTrackerDBSteps.retornaResumoDaTarefaDB(bugID));

    }

    @E("preencho os campos obrigatórios do Relatório exceto CATEGORIA")
    public void preenchoOsCamposObrigatóriosDoRelatórioExcetoCATEGORIA() throws IOException {
        Lorem lorem = LoremIpsum.getInstance();
        String resumo = "Teste de Resumo " + RandomStringUtils.randomAlphanumeric(5, 10);
        Serenity.setSessionVariable("resumo").to(resumo);
        String descricao = "Teste de descricao " + lorem.getParagraphs(1, 3);
        bugTrackerSteps.preenchoOsCamposDoRelatórioExcetoCateogria(resumo, descricao);

    }

    @Entao("valido mensagem de erro ao tentar inserir tarefa sem uma categoria")
    public void validoMensagemDeErroAoTentarInserirTarefaSemUmaCategoria() throws IOException {

        Assert.assertEquals(Constants.CATEGORY_ERROR, bugTrackerSteps.retornaMensagemErroCategoriaNaoSelecionada());

    }

    @Dado("que tenho uma tarefa registrada")
    public void queTenhoUmaTarefaRegistrada() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        Lorem lorem = LoremIpsum.getInstance();

        dashBoardSteps.acessoATelaCriarTarefa();

        String categoria = "[Todos os Projetos] General";
        String resumo = "Teste de Resumo " + RandomStringUtils.randomAlphanumeric(5, 10);
        Serenity.setSessionVariable("resumo").to(resumo);
        String descricao = "Teste de descricao " + lorem.getParagraphs(1, 3);
        bugTrackerSteps.preenchoOsCamposDoRelatórioCom(categoria, resumo, descricao);
        bugTrackerSteps.clicoEmCriarNovaTarefa();

        String bugID = bugTrackerSteps.retornaTextoBugID();

        String bugIDParaBD = bugID.replaceAll("^0+(?!$)", "");
        Assert.assertTrue(bugTrackerDBSteps.retornaResumoDaTarefaDB(bugIDParaBD).contains(Serenity.sessionVariableCalled("resumo")));

        Serenity.setSessionVariable("categoriaDuplicada").to(categoria);
        Serenity.setSessionVariable("resumoDuplicada").to(resumo);
        Serenity.setSessionVariable("descricaoDuplicada").to(descricao);
        Serenity.setSessionVariable("bugID").to(bugID);

    }
    @E("crio uma segunda tarefa")
    public void crioUmaSegundaTarefa() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        Lorem lorem = LoremIpsum.getInstance();

        dashBoardSteps.acessoATelaCriarTarefaSemClicarEmSelecionarProjeto();

        String categoria = "[Todos os Projetos] General";
        String resumo = "Teste de Resumo " + RandomStringUtils.randomAlphanumeric(5, 10);
        Serenity.setSessionVariable("resumo").to(resumo);
        String descricao = "Teste de descricao " + lorem.getParagraphs(1, 3);
        bugTrackerSteps.preenchoOsCamposDoRelatórioCom(categoria, resumo, descricao);
        bugTrackerSteps.clicoEmCriarNovaTarefa();

        String bugID = bugTrackerSteps.retornaTextoBugID();

        String bugIDParaBD = bugID.replaceAll("^0+(?!$)", "");
        Assert.assertTrue(bugTrackerDBSteps.retornaResumoDaTarefaDB(bugIDParaBD).contains(Serenity.sessionVariableCalled("resumo")));

        Serenity.setSessionVariable("bugIDSecundaria").to(bugID);

    }

    @E("crio uma tarefa Duplicada")
    public void crioUmaTarefaDuplicada() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        Lorem lorem = LoremIpsum.getInstance();

        dashBoardSteps.acessoATelaCriarTarefaSemClicarEmSelecionarProjeto();

        String categoria = Serenity.sessionVariableCalled("categoriaDuplicada");
        String resumo = Serenity.sessionVariableCalled("resumoDuplicada");
        Serenity.setSessionVariable("resumo").to(resumo);
        String descricao = Serenity.sessionVariableCalled("descricaoDuplicada");
        bugTrackerSteps.preenchoOsCamposDoRelatórioCom(categoria, resumo, descricao);
        bugTrackerSteps.clicoEmCriarNovaTarefa();

        String bugID = bugTrackerSteps.retornaTextoBugID();

        String bugIDParaBD = bugID.replaceAll("^0+(?!$)", "");
        Assert.assertTrue(bugTrackerDBSteps.retornaResumoDaTarefaDB(bugIDParaBD).contains(Serenity.sessionVariableCalled("resumo")));

        Serenity.setSessionVariable("bugIDSecundaria").to(bugID);


    }
}
