package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.dbsteps.UsersDBSteps;
import br.com.serenitybddtemplate.steps.GerenciarSteps;
import br.com.serenitybddtemplate.steps.GerenciarUsuariosSteps;
import br.com.serenitybddtemplate.utils.Constants;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.annotations.Steps;
import org.junit.Assert;

import java.io.IOException;
import java.util.ArrayList;

public class GerenciarUsuariosStepDefinitions {
    @Steps
    GerenciarUsuariosSteps gerenciarUsuariosSteps;
    @Steps
    GerenciarSteps gerenciarSteps;

    @E("preencho os campos com um {string},{string},{string},{string}")
    public void clicoEmGerenciarUsuarios(String usuario, String nomeVerdadeiro, String email, String nivelAcesso) {

        gerenciarUsuariosSteps.preenchoInformacoesDoUsuarioParaCadastro(usuario, nomeVerdadeiro, email, nivelAcesso);
    }

    @E("clico em Criar Nova Conta")
    public void clicoEmCriarNovaConta() {

        gerenciarUsuariosSteps.clicoEmCriarNovaConta();
    }

    @Entao("valido que o usuário com nome {string} foi cadastrado com sucesso")
    public void validoQueOUsuarioComNomeFoiCadastradoComSucesso(String usuario) {
        UsersDBSteps userDB = new UsersDBSteps();
        gerenciarSteps.clicoEmGerenciarUsuarios();
        String userReturnDB = userDB.validaNomeUsuarioDB(usuario);
        String usuarioNaTabela = gerenciarUsuariosSteps.retornaUsuarioNaTabela(usuario);
        System.out.println(userReturnDB);
        Assert.assertEquals(usuarioNaTabela, userReturnDB);

    }


    @E("seleciono um usuario da tabela")
    public void selecionoUmUsuarioDaTabela() throws IOException {
        String usuario = "user1";
        Serenity.setSessionVariable("usuario").to(usuario);
        gerenciarUsuariosSteps.selecionoUmUsuarioDaTabela(usuario);

    }

    @E("mudo o nivel dele para Desenvolvedor")
    public void mudoONivelDeleParaDesenvolvedor() throws IOException {
        String nivelDeAcesso = "desenvolvedor";
        gerenciarUsuariosSteps.mudoONivelDeAcesso(nivelDeAcesso);

    }

    @E("mudo o nivel dele para Atualizador")
    public void mudoONivelDeleParaAtualizador() throws IOException {
        String nivelDeAcesso = "atualizador";
        gerenciarUsuariosSteps.mudoONivelDeAcesso(nivelDeAcesso);

    }

    @E("mudo o nivel dele para Relator")
    public void mudoONivelDeleParaRelator() throws IOException {
        String nivelDeAcesso = "relator";
        gerenciarUsuariosSteps.mudoONivelDeAcesso(nivelDeAcesso);

    }

    @E("mudo o nivel dele para Visualizador")
    public void mudoONivelDeleParaVisualizador() throws IOException {
        String nivelDeAcesso = "visualizador";
        gerenciarUsuariosSteps.mudoONivelDeAcesso(nivelDeAcesso);

    }

    @E("mudo o nivel dele para Administrador")
    public void mudoONivelDeleParaAdministrador() throws IOException {
        String nivelDeAcesso = "administrador";
        gerenciarUsuariosSteps.mudoONivelDeAcesso(nivelDeAcesso);

    }

    @E("mudo o nivel dele para Gerente")
    public void mudoONivelDeleParaGerente() throws IOException {
        String nivelDeAcesso = "gerente";
        gerenciarUsuariosSteps.mudoONivelDeAcesso(nivelDeAcesso);

    }

    @E("clico em Atualizar Usuário")
    public void clicoEmAtualizarUsuario() {
        gerenciarUsuariosSteps.clicoEmAtualizarUsuario();
    }

    @Entao("valido no banco de dados o nivel de acesso do usuario igual a Desenvolvedor")
    public void validoNoBancoDeDadosONivelDeAcessoDoUsuarioDesenvolvedor() throws IOException {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String usuario = Serenity.sessionVariableCalled("usuario");
        String accessLevel = usersDBSteps.retornaUserCompletoPorNome(usuario).get(7);
        Assert.assertEquals("55", accessLevel);


    }

    @Entao("valido no banco de dados o nivel de acesso do usuario igual a Atualizador")
    public void validoNoBancoDeDadosONivelDeAcessoDoUsuarioIgualAAtualizador() throws IOException {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String usuario = Serenity.sessionVariableCalled("usuario");
        String accessLevel = usersDBSteps.retornaUserCompletoPorNome(usuario).get(7);
        Assert.assertEquals("40", accessLevel);

    }

    @Entao("valido no banco de dados o nivel de acesso do usuario igual a Relator")
    public void validoNoBancoDeDadosONivelDeAcessoDoUsuarioIgualARelator() throws IOException {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String usuario = Serenity.sessionVariableCalled("usuario");
        String accessLevel = usersDBSteps.retornaUserCompletoPorNome(usuario).get(7);
        Assert.assertEquals("25", accessLevel);

    }

    @Entao("valido no banco de dados o nivel de acesso do usuario igual a Visualizador")
    public void validoNoBancoDeDadosONivelDeAcessoDoUsuarioIgualAVisualizador() throws IOException {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String usuario = Serenity.sessionVariableCalled("usuario");
        String accessLevel = usersDBSteps.retornaUserCompletoPorNome(usuario).get(7);
        Assert.assertEquals("10", accessLevel);

    }

    @Entao("valido no banco de dados o nivel de acesso do usuario igual a Administrador")
    public void validoNoBancoDeDadosONivelDeAcessoDoUsuarioIgualAAdministrador() throws IOException {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String usuario = Serenity.sessionVariableCalled("usuario");
        String accessLevel = usersDBSteps.retornaUserCompletoPorNome(usuario).get(7);
        Assert.assertEquals("90", accessLevel);

    }

    @Entao("valido no banco de dados o nivel de acesso do usuario igual a Gerente")
    public void validoNoBancoDeDadosONivelDeAcessoDoUsuarioIgualAGerente() throws IOException {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String usuario = Serenity.sessionVariableCalled("usuario");
        String accessLevel = usersDBSteps.retornaUserCompletoPorNome(usuario).get(7);
        Assert.assertEquals("70", accessLevel);

    }


    @E("desmarco a caixa Habilitado")
    public void desmarcoACaixaHabilitado() throws IOException {
        gerenciarUsuariosSteps.desmarcoACaixaHabilitado();

    }

    @Entao("valido no banco de dados se o usuario foi desabilitado")
    public void validoNoBancoDeDadosSeOUsuarioFoiDesabilitado() throws IOException {
        String usuario = Serenity.sessionVariableCalled("usuario");
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String atividadeUsuario = usersDBSteps.retornaUserCompletoPorNome(usuario).get(5);
        Assert.assertEquals("0", atividadeUsuario);


    }

    @E("marco a caixa de Protegido")
    public void marcoACaixaDeProtegido() throws IOException {
        gerenciarUsuariosSteps.marcoACaixaDeProtegido();

    }

    @Entao("valido no banco de dados se o usuario tem o status de Protegido")
    public void validoNoBancoDeDadosSeOUsuarioTemOStatusDeProtegido() throws IOException {
        String usuario = Serenity.sessionVariableCalled("usuario");
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        String atividadeUsuario = usersDBSteps.retornaUserCompletoPorNome(usuario).get(6);
        Assert.assertEquals("1", atividadeUsuario);


    }

    @Entao("valido mensagem de erro que o email inserido é invalido")
    public void validoMensagemDeErroQueOEmailInseridoEInvalido() throws IOException {

        Assert.assertEquals(Constants.CADASTRO_USUARIO_ERRO_EMAIL_INVALIDO, gerenciarUsuariosSteps.retornaMensagemErro());

    }

    @Entao("valido mensagem de erro que o nome de usuário é invalido")
    public void validoMensagemDeErroQueONomeDeUsuarioEInvalido() throws IOException {
        Assert.assertEquals(Constants.CADASTRO_USUARIO_ERRO_NOME_INVALIDO, gerenciarUsuariosSteps.retornaMensagemErro());


    }

    @E("clico em Apagar Usuario")
    public void clicoEmApagarUsuario() throws IOException {
        gerenciarUsuariosSteps.clicoEmApagarUsuario();

    }

    @E("confirmo a exclusão do usuário")
    public void confirmoAExclusaoDoUsuario() throws IOException {
        gerenciarUsuariosSteps.confirmoAExclusaoDoUsuario();


    }

    @Entao("valido no banco de dados que o usuário foi excluido com sucesso")
    public void validoNoBancoDeDadosQueOUsuarioFoiExcluidoComSucesso() throws IOException {
        UsersDBSteps userDB = new UsersDBSteps();
        String usuarioExcluido = Serenity.sessionVariableCalled("usuario");
        ArrayList<String> userReturnDB = userDB.retornaUsuarioPorNome(usuarioExcluido);
        Assert.assertNull(userReturnDB);

    }
}
