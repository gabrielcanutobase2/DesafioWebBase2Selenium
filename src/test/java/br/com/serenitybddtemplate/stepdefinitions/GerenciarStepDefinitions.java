package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.steps.GerenciarSteps;
import io.cucumber.java.pt.E;
import net.serenitybdd.annotations.Steps;

import java.io.IOException;

public class GerenciarStepDefinitions {
    @Steps
    GerenciarSteps gerenciarSteps;

    @E("clico em Gerenciar Usuarios")
    public void clicoEmGerenciarUsuarios() throws IOException {
        gerenciarSteps.clicoEmGerenciarUsuarios();

    }

    @E("clico em Gerenciar Projetos")
    public void clicoEmGerenciarProjetos() throws IOException {
        gerenciarSteps.clicoEmGerenciarProjetos();

    }

    @E("clico em Gerenciar Marcadores")
    public void clicoEmGerenciarMarcadores() throws IOException {
        gerenciarSteps.clicoEmGerenciarMarcadores();

    }


}
