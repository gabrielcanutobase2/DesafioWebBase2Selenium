package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.dbsteps.UsersDBSteps;
import br.com.serenitybddtemplate.steps.DashBoardSteps;
import br.com.serenitybddtemplate.steps.LoginSteps;
import br.com.serenitybddtemplate.utils.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.annotations.Steps;
import org.junit.Assert;

import java.io.IOException;

public class LoginStepDefinitions {
    @Steps
    LoginSteps loginSteps;

    @Steps
    DashBoardSteps dashBoardSteps;

    @Dado("que informo o nome de usuario {string}")
    public void queInformoONomeDeUsuario(String usuario) throws IOException {
        Serenity.setSessionVariable("usuario").to(usuario);
        loginSteps.preencherUsuario(usuario);

    }

    @E("clico em Entrar com usuário")
    public void clicoEmEntrar() throws IOException {
        loginSteps.clicarEmEntrarComUsuario();


    }

    @E("informo a senha {string}")
    public void preencherSenha(String senha) throws IOException {

        Serenity.setSessionVariable("senha").to(senha);
        loginSteps.preencherSenha(senha);


    }

    @Quando("acesso a minha conta")
    public void acessarProjeto() throws IOException {
        loginSteps.clicarEmLogar();

    }


    @Entao("o usuário {string} deve ser autenticado com sucesso")
    public void verificarSeUsuarioEstaAutenticado(String usuario) throws IOException {

        Assert.assertEquals(usuario, dashBoardSteps.retornaUsuarioDasInformacoesDeLogin());

    }


    @Dado("que acesso o site como {string}")
    public void queAcessoOSiteComoAdministrador(String statusUsuario) throws IOException {
        loginSteps.AcessoOSiteComo(statusUsuario);

    }

    @Entao("o sistema retorna uma mensagem de erro de login")
    public void oSistemaRetornaUmaMensagemDeErroDeLogin() throws IOException {
        Assert.assertEquals(Constants.LOGIN_ERROR, loginSteps.retornaMensagemDeErro());

    }


    @Dado("que tenho um usario inativo no banco")
    public void queTenhoUmUsarioInativoNoBanco() throws IOException {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        usersDBSteps.CarregaUmUsuarioInativo();
        Assert.assertEquals(0, usersDBSteps.retornaAtividadeUsuarioDB("usuario.desativado"));

    }

    @Dado("que informo o nome de usuario {string} por Javascript")
    public void queInformoONomeDeUsuarioPorJavascript(String usuario) throws IOException {
        loginSteps.queInformoONomeDeUsuarioPorJavascript(usuario);

    }

    @E("clico em Entrar por Javascript")
    public void clicoEmEntrarPorJavascript() throws IOException {
        loginSteps.clicoEmEntrarComUsuarioPorJavascript();

    }

    @E("informo a senha {string} por Javascript")
    public void informoASenhaPorJavascript(String senha) throws IOException {
        loginSteps.informoASenhaPorJavascript(senha);


    }
}
