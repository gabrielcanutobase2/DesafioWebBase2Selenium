package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.steps.DashBoardSteps;
import br.com.serenitybddtemplate.steps.GerenciarProjetosSteps;
import br.com.serenitybddtemplate.steps.GerenciarSteps;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import net.serenitybdd.annotations.Steps;
import org.junit.Assert;

import java.io.IOException;

public class GerenciarProjetosStepDefinitions {
    @Steps
    GerenciarProjetosSteps gerenciarProjetosSteps;
    @Steps
    DashBoardSteps dashBoardSteps;


    @Steps
    GerenciarSteps gerenciarSteps;


    @E("clico em Criar Novo Projeto")
    public void clicoEmCriarNovoProjeto() throws IOException {
        gerenciarProjetosSteps.clicoEmCriarNovoProjeto();

    }

    @E("preencho os campos do projeto com um {string},{string},{string},{string}")
    public void preenchoOsCamposDoProjetoComUm(String nomeProjeto, String statusProjeto, String visibilidadeProjeto, String descricaoProjeto) throws IOException {
        gerenciarProjetosSteps.preenchoInformacoesDoProjeto(nomeProjeto, statusProjeto, visibilidadeProjeto, descricaoProjeto);

    }

    @Entao("valido que o projeto com nome {string} foi cadastrado com sucesso")
    public void validoQueOProjetoComNomeFoiCadastradoComSucesso(String nomeProjeto) throws IOException {
        Assert.assertTrue(gerenciarProjetosSteps.retornaBooleanProjetoNaTabela(nomeProjeto));

    }

    @E("preencho o campo de Categoria com {string}")
    public void preenchoOCampoDeCategoria(String nomeCategoria) throws IOException {
        gerenciarProjetosSteps.preenchoOCampoDeCategoria(nomeCategoria);


    }

    @E("clico em Adicionar Categoria")
    public void clicoEmAdicionarCategoria() throws IOException {
        gerenciarProjetosSteps.clicoEmAdicionarCategoria();

    }

    @Entao("valido que a categoria com nome {string} foi cadastrada com sucesso")
    public void validoQueACategoriaComNomeFoiCadastradaComSucesso(String nomeCategoria) throws IOException {
        gerenciarProjetosSteps.validoQueACategoriaComNomeFoiCadastradaComSucesso(nomeCategoria);

    }

    @Dado("que tenho Categorias criadas")
    public void queTenhoCategoriasCriadas() throws IOException {
        String nomeCategoria = "CategoriaTeste1";
        dashBoardSteps.acessoTelaGerenciar();
        gerenciarSteps.clicoEmGerenciarProjetos();
        gerenciarProjetosSteps.preenchoOCampoDeCategoria(nomeCategoria);
        gerenciarProjetosSteps.clicoEmAdicionarCategoria();
        gerenciarProjetosSteps.validoQueACategoriaComNomeFoiCadastradaComSucesso(nomeCategoria);

    }
}
