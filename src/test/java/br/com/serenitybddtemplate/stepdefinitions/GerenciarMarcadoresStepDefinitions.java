package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.dbsteps.BugTrackerDBSteps;
import br.com.serenitybddtemplate.steps.DashBoardSteps;
import br.com.serenitybddtemplate.steps.GerenciarMarcadoresSteps;
import br.com.serenitybddtemplate.steps.GerenciarSteps;
import br.com.serenitybddtemplate.utils.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.annotations.Steps;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;

import java.io.IOException;
import java.util.ArrayList;

public class GerenciarMarcadoresStepDefinitions {
    @Steps
    DashBoardSteps dashBoardSteps;
    @Steps
    GerenciarMarcadoresSteps gerenciarMarcadoresSteps;
    @Steps
    GerenciarSteps gerenciarSteps;

    @E("clico em Criar Novo Marcador")
    public void clicoEmCriarNovoMarcador() throws IOException {
        gerenciarMarcadoresSteps.clicoEmCriarNovoMarcador();

    }

    @E("insiro o nome e uma descricao para o marcador")
    public void insiroONomeEUmaDescricaoParaOMarcador() throws IOException {
        String novoNomeMarcador = "Marcador Teste " + RandomStringUtils.randomNumeric(1, 3);
        Serenity.setSessionVariable("novoNomeMarcador").to(novoNomeMarcador);
        String novaDescricaoMarcador = "Descricao marcador Teste " + RandomStringUtils.randomNumeric(5, 10);
        gerenciarMarcadoresSteps.insiroONomeEUmaDescricaoParaOMarcador(novoNomeMarcador, novaDescricaoMarcador);

    }

    @E("clico em Criar Marcador")
    public void clicoEmCriarMarcador() throws IOException {
        gerenciarMarcadoresSteps.clicoEmCriarMarcador();

    }

    @Entao("valido no banco que o marcador foi criado com sucesso")
    public void validoNoBancoQueOMarcadorFoiCriadoComSucesso() throws IOException {

        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String nomeMarcador = Serenity.sessionVariableCalled("novoNomeMarcador");
        ArrayList<String> marcadorAtualizadoBD = bugTrackerDBSteps.retornaMarcadorPorNome(nomeMarcador);
        Assert.assertNotNull(marcadorAtualizadoBD);
        Assert.assertEquals(nomeMarcador, marcadorAtualizadoBD.get(2));

    }

    @E("seleciono um marcador da tabela")
    public void selecionoUmMarcadorDaTabela() throws IOException {
        String nomeMarcador = "tagTeste";
        Serenity.setSessionVariable("nomeMarcadorPadrao").to(nomeMarcador);
        gerenciarMarcadoresSteps.selecionoUmMarcadorDaTabela(nomeMarcador);


    }

    @E("seleciono um marcador criado na tabela")
    public void selecionoUmMarcadorCriadoDaTabela() throws IOException {
        String nomeMarcador = Serenity.sessionVariableCalled("nomeMarcadorExistente");

        gerenciarMarcadoresSteps.selecionoUmMarcadorDaTabela(nomeMarcador);


    }

    @E("altero o nome do marcador para um novo")
    public void alteroONomeDoMarcadorParaUmNovo() throws IOException {
        gerenciarMarcadoresSteps.clicoEmAtualizarMarcadorParaEditar();
        String nomeMarcadorAtualizado = "tagTesteAtualizado";
        Serenity.setSessionVariable("nomeMarcadorAtualizado").to(nomeMarcadorAtualizado);
        gerenciarMarcadoresSteps.alteroONomeDoMarcadorParaUmNovo(nomeMarcadorAtualizado);

    }

    @E("clico em Atualizar Marcador")
    public void clicoEmAtualizarMarcador() throws IOException {
        gerenciarMarcadoresSteps.clicoEmAtualizarMarcador();

    }

    @Entao("valido no banco que o marcador mudou de nome")
    public void validoNoBancoQueOMarcadorMudouDeNome() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String nomeAtualizadoMarcador = Serenity.sessionVariableCalled("nomeMarcadorAtualizado");
        ArrayList<String> marcadorAtualizadoBD = bugTrackerDBSteps.retornaMarcadorPorNome(nomeAtualizadoMarcador);
        Assert.assertNotNull(marcadorAtualizadoBD);
        Assert.assertEquals(nomeAtualizadoMarcador, marcadorAtualizadoBD.get(2));

    }


    @E("altero o nome do marcador para o de outro marcador existente")
    public void alteroONomeDoMarcadorParaODeOutroMarcadorExistente() {
        gerenciarMarcadoresSteps.clicoEmAtualizarMarcadorParaEditar();
        String nomeMarcadorAtualizado = "tagTeste2";
        Serenity.setSessionVariable("nomeMarcadorIgual").to(nomeMarcadorAtualizado);
        gerenciarMarcadoresSteps.alteroONomeDoMarcadorParaUmNovo(nomeMarcadorAtualizado);
    }

    @Entao("valido mensagem de erro nome do marcador nao pode ser igual a de existente")
    public void validoMensagemDeErroNomeDoMarcadorNaoPodeSerIgualADeExistente() throws IOException {
        Assert.assertEquals(Constants.GERENCIAR_MARCADOR_ERRO_MARCADOR_JA_EXISTE, gerenciarMarcadoresSteps.retornaMensagemDeErro());

    }


    @E("clico em Apagar")
    public void clicoEmApagar() throws IOException {
        gerenciarMarcadoresSteps.clicoEmApagar();
    }

    @E("clico novamente em Apagar para confirmar a exclusao")
    public void clicoNovamenteEmApagarParaConfirmarAExclusao() throws IOException {
        gerenciarMarcadoresSteps.clicoNovamenteEmApagarParaConfirmarAExclusao();

    }

    @Entao("verifico pelo nome no banco de dados se o marcador foi excluido com sucesso")
    public void verificoPeloNomeNoBancoDeDadosSeOMarcadorFoiExcluidoComSucesso() throws IOException {
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        String nomeMarcador = Serenity.sessionVariableCalled("nomeMarcadorExistente");
        ArrayList<String> marcadorAtualizadoBD = bugTrackerDBSteps.retornaMarcadorPorNome(nomeMarcador);
        Assert.assertNull(marcadorAtualizadoBD);

    }

    @Dado("que possuo um marcador existente")
    public void possuoUmMarcadorExistente() throws IOException {
        dashBoardSteps.acessoTelaGerenciar();
        gerenciarSteps.clicoEmGerenciarMarcadores();
        gerenciarMarcadoresSteps.clicoEmCriarNovoMarcador();
        String novoNomeMarcador = "Marcador Teste " + RandomStringUtils.randomNumeric(1, 3);
        Serenity.setSessionVariable("nomeMarcadorExistente").to(novoNomeMarcador);
        String novaDescricaoMarcador = "Descricao marcador Teste " + RandomStringUtils.randomNumeric(5, 10);
        gerenciarMarcadoresSteps.insiroONomeEUmaDescricaoParaOMarcador(novoNomeMarcador, novaDescricaoMarcador);
        gerenciarMarcadoresSteps.clicoEmCriarMarcador();

    }
}
