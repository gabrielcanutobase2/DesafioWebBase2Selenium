package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.steps.DashBoardSteps;
import br.com.serenitybddtemplate.steps.LoginSteps;
import io.cucumber.java.pt.Quando;
import net.serenitybdd.annotations.Steps;

import java.io.IOException;


public class DashBoardStepDefinitions {
    @Steps
    LoginSteps loginSteps;

    @Steps
    DashBoardSteps dashBoardSteps;

    @Quando("acesso a tela Gerenciar")
    public void acessoATelaGerenciar() throws IOException {

        dashBoardSteps.acessoTelaGerenciar();

    }

    @Quando("acesso a tela Ver Tarefa")
    public void acessoTelaVerTarefa() throws IOException {
        dashBoardSteps.acessoTelaVerTarefa();


    }

    @Quando("acessoa a tela Criar Tarefa")
    public void acessoaATelaCriarTarefa() throws IOException {
        dashBoardSteps.acessoATelaCriarTarefa();
    }

    @Quando("acesso a tela Minha Visao")
    public void acessoATelaMinhaVisao() throws IOException {
        dashBoardSteps.acessoATelaMinhaVisao();


    }
}
