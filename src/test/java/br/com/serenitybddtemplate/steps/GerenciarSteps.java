package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.GerenciarPage;
import net.serenitybdd.annotations.Step;

public class GerenciarSteps {
    GerenciarPage gerenciarPage;

    @Step("clico em Gerenciar Usuarios")
    public void clicoEmGerenciarUsuarios() {
        gerenciarPage.ClicarEmGerenciarUsuarios();
    }

    @Step("clico em Gerenciar Projetos")
    public void clicoEmGerenciarProjetos() {
        gerenciarPage.ClicarEmGerenciarProjetos();
    }

    @Step("clico em Gerenciar Marcadores")
    public void clicoEmGerenciarMarcadores() {
        gerenciarPage.ClicarEmGerenciarMarcadores();
    }

}
