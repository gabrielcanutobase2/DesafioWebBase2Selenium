package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.GerenciarProjetoPage;
import net.serenitybdd.annotations.Step;

public class GerenciarProjetosSteps {
    GerenciarProjetoPage gerenciarProjetoPage;

    @Step("clico em Criar Novo Projeto")
    public void clicoEmCriarNovoProjeto() {
        gerenciarProjetoPage.ClicarEmCriarNovoProjeto();
    }

    @Step("preencho os campos do projeto com um {0},{1},{2},{3}")
    public void preenchoInformacoesDoProjeto(String nomeProjeto, String statusProjeto, String visibilidadeProjeto, String descricaoProjeto) {
        gerenciarProjetoPage.CriarNovoProjeto(nomeProjeto, statusProjeto, visibilidadeProjeto, descricaoProjeto);
        gerenciarProjetoPage.ClicarEmAdicionarProjeto();
    }

    @Step("valido que o projeto com nome {0} foi cadastrado com sucesso")
    public boolean retornaBooleanProjetoNaTabela(String projeto) {
        return gerenciarProjetoPage.retornaValidacaoNomeTabela(projeto);
    }


    @Step("preencho o campo de Categoria com {0}")
    public void preenchoOCampoDeCategoria(String nomeCategoria) {
        gerenciarProjetoPage.AdicionarNovaCategoria(nomeCategoria);
    }

    @Step("clico em Adicionar Categoria")
    public void clicoEmAdicionarCategoria() {
        gerenciarProjetoPage.ClicarEmAdicionarNovaCategoria();
    }

    @Step("valido que a categoria com nome {string} foi cadastrada com sucesso")
    public boolean validoQueACategoriaComNomeFoiCadastradaComSucesso(String nomeCategoria) {
        return gerenciarProjetoPage.retornaValidacaoNomeTabela(nomeCategoria);
    }
}
