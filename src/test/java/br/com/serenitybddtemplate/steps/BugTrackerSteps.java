package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.BugTrackerPage;
import net.serenitybdd.annotations.Step;


public class BugTrackerSteps {

    BugTrackerPage bugTrackerPage;

    @Step("preencho os campos obrigatórios do Relatório com {0},{1} ,{2}")
    public void preenchoOsCamposDoRelatórioCom(String categoria, String resumo, String descricao) {
        bugTrackerPage.SelecionoAOpcaoDeCategoria(categoria);
        bugTrackerPage.InsiroInformacoesNoCampoResumo(resumo);
        bugTrackerPage.InsiroInformacoesNoCampoDescricao(descricao);

    }

    @Step("preencho os campos obrigatórios do Relatório com {0} e {1} exceto CATEGORIA")
    public void preenchoOsCamposDoRelatórioExcetoCateogria(String resumo, String descricao) {
        bugTrackerPage.InsiroInformacoesNoCampoResumo(resumo);
        bugTrackerPage.InsiroInformacoesNoCampoDescricao(descricao);
    }

    @Step("clico em Criar Nova tarefa")
    public void clicoEmCriarNovaTarefa() {
        bugTrackerPage.ClickBotaoCriarNovaTarefa();

    }

    public String retornaMensagemErroCategoriaNaoSelecionada() {
        return bugTrackerPage.retornaErroCategoriaNaoInserida();
    }

    public String retornaTextoBugID() {
        return bugTrackerPage.retornaBugIDTexto();

    }
}
