package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.DashBoardPage;
import net.serenitybdd.annotations.Step;

public class DashBoardSteps {
    DashBoardPage dashBoardPage;

    @Step("Retorna username das informações de login")
    public String retornaUsuarioDasInformacoesDeLogin() {
        return dashBoardPage.retornaNomeUsuarioEmTela();
    }

    @Step("acesso a tela Gerenciar")
    public void acessoTelaGerenciar() {
        dashBoardPage.ClicarEmGerenciar();
    }

    @Step("acesso a tela CriarTarefa")
    public void acessoATelaCriarTarefa() {
        dashBoardPage.ClicarEmCriarTarefa();
        dashBoardPage.ClicarBotaoSelecionarProjeto();
    }

    @Step("acesso a tela CriarTarefa sem selecionar o projeto")
    public void acessoATelaCriarTarefaSemClicarEmSelecionarProjeto() {
        dashBoardPage.ClicarEmCriarTarefa();

    }

    @Step("acesso a tela Ver Tarefa")
    public void acessoTelaVerTarefa() {
        dashBoardPage.ClicarEmVerTarefas();
    }

    @Step("acesso a tela Minha Visao")
    public void acessoATelaMinhaVisao() {
        dashBoardPage.ClicarEmMinhaVisao();
    }

}
