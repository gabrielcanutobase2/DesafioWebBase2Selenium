package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.LoginPage;
import br.com.serenitybddtemplate.utils.Utils;
import net.serenitybdd.annotations.Step;
import net.thucydides.junit.annotations.UseTestDataFrom;

@UseTestDataFrom(value = "src/test/resources/DataDriven/Files")
public class LoginSteps {

    LoginPage loginPage;

    @Step("Retorna mensagem de erro")
    public String retornaMensagemDeErro() {
        return loginPage.retornaMensagemDeErro();
    }

    @Step("Acesso a pagina de login")
    public void abrirPaginaLogin() {
        loginPage.open();
    }

    @Step("informo o nome de usuario {0}")
    public void preencherUsuario(String usuario) {
        loginPage.preencherUsuario(usuario);
    }


    @Step("informo a senha {0}")
    public void preencherSenha(String senha) {
        loginPage.preencherSenha(senha);
    }


    @Step("clico em Entrar com usuario")
    public void clicarEmEntrarComUsuario() {
        loginPage.clicarEmEntrarComUsuario();
    }

    @Step("clico em Logar")
    public void clicarEmLogar() {
        loginPage.clicarEmLogar();
    }


    @Step("Acesso o site como {0}")
    public void AcessoOSiteComo(String tipoAcesso) {
        if (tipoAcesso.equalsIgnoreCase("administrador")) {
            loginPage.preencherUsuario(Utils.User());
            loginPage.clicarEmEntrarComUsuario();
            loginPage.preencherSenha(Utils.Password());
            loginPage.clicarEmLogar();

        }
    }

    @Step("que informo o nome de usuario {0} por Javascript")
    public void queInformoONomeDeUsuarioPorJavascript(String usuario) {
        loginPage.preencherUsuarioJavasCript(usuario);

    }

    @Step("clico em Entrar com usuário por Javascript")
    public void clicoEmEntrarComUsuarioPorJavascript() {
        loginPage.clicarEmEntrarComUsuarioJavascript();
    }

    @Step("clico em Logar com javascript")
    public void clicarEmLogarComJavascript() {
        loginPage.clicarEmLogarJavascript();
    }

    @Step("informo a senha {0} por Javascript")
    public void informoASenhaPorJavascript(String senha) {
        loginPage.preencherSenhaJavaScript(senha);
    }
}
