package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.GerenciarUsuarioPage;
import net.serenitybdd.annotations.Step;

public class GerenciarUsuariosSteps {
    GerenciarUsuarioPage gerenciarUsuarioPage;

    @Step("clico em Criar Nova Conta")
    public void clicoEmCriarNovaConta() {
        gerenciarUsuarioPage.ClicarEmCriarNovaConta();
    }

    @Step("preencho os campos com um {0},{1},{2},{3}")
    public void preenchoInformacoesDoUsuarioParaCadastro(String usuario, String nomeReal, String email, String nivelAcesso) {
        gerenciarUsuarioPage.CriarNovoUsuario(usuario, nomeReal, email, nivelAcesso);
        gerenciarUsuarioPage.IncluirUsuario();
    }

    @Step("valido que o usuário com nome {0} foi cadastrado com sucesso")
    public String retornaUsuarioNaTabela(String usuario) {
        return gerenciarUsuarioPage.retornaNomeTabela(usuario);
    }

    @Step("seleciono um usuario da tabela")
    public void selecionoUmUsuarioDaTabela(String usuario) {
        gerenciarUsuarioPage.ClicaUsuarioNaTabela(usuario);
    }

    @Step("mudo o nivel de acesso dele")
    public void mudoONivelDeAcesso(String nivelAcesso) {
        gerenciarUsuarioPage.AtualizaNivelDeAcesso(nivelAcesso);

    }

    @Step("clico em Atualizar Usuário")
    public void clicoEmAtualizarUsuario() {
        gerenciarUsuarioPage.AtualizarUsuario();

    }

    @Step("mudo desmarco a caixa Habilitado")
    public void desmarcoACaixaHabilitado() {
        gerenciarUsuarioPage.ClicaCheckBoxHabilitarDesabilitarUsuario();
    }

    @Step("marco a caixa de Protegido")
    public void marcoACaixaDeProtegido() {
        gerenciarUsuarioPage.ClicaCheckBoxUsuarioProtegido();
    }

    public String retornaMensagemErro() {
        return gerenciarUsuarioPage.retornaMensagemErro();
    }

    @Step("clico em Apagar Usuario")
    public void clicoEmApagarUsuario() {
        gerenciarUsuarioPage.ApagarUsuario();

    }

    @Step("confirmo a exclusão do usuário")
    public void confirmoAExclusaoDoUsuario() {
        gerenciarUsuarioPage.ConfirmarApagarUsuario();
    }

}
