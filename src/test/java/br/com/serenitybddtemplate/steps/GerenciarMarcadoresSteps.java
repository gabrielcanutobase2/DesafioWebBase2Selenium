package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.GerenciarMarcadoresPage;
import net.serenitybdd.annotations.Step;

public class GerenciarMarcadoresSteps {
    GerenciarMarcadoresPage gerenciarMarcadoresPage;

    @Step("clico em Criar Novo Marcador")
    public void clicoEmCriarNovoMarcador() {
        gerenciarMarcadoresPage.ClicarEmCriarNovoMarcador();
    }

    @Step("insiro o nome e uma descricao para o marcador")
    public void insiroONomeEUmaDescricaoParaOMarcador(String nome, String descricao) {
        gerenciarMarcadoresPage.PreencheDadosNovoMarcador(nome, descricao);
    }

    @Step("clico em Criar Marcador")
    public void clicoEmCriarMarcador() {
        gerenciarMarcadoresPage.IncluirMarcador();
    }

    @Step("seleciono um marcador da tabela")
    public void selecionoUmMarcadorDaTabela(String nomeMarcador) {
        gerenciarMarcadoresPage.SelecionaMarcadorNaTabela(nomeMarcador);

    }

    @Step("Clico em Ataulizar para Editar marcador")
    public void clicoEmAtualizarMarcadorParaEditar() {
        gerenciarMarcadoresPage.AtualizarMarcador();

    }

    @Step("altero o nome do marcador para um novo")
    public void alteroONomeDoMarcadorParaUmNovo(String marcador) {
        gerenciarMarcadoresPage.AtualizarNomeMarcador(marcador);

    }

    @Step("clico em Atualizar Marcador")
    public void clicoEmAtualizarMarcador() {
        gerenciarMarcadoresPage.ConfirmarAtualizarMarcador();
    }

    public String retornaMensagemDeErro() {
        return gerenciarMarcadoresPage.retornaMensagemErro();
    }

    @Step("clico em Apagar")
    public void clicoEmApagar() {
        gerenciarMarcadoresPage.ClicarEmApagar();

    }

    @Step("clico novamente em Apagar para confirmar a exclusao")
    public void clicoNovamenteEmApagarParaConfirmarAExclusao() {
        gerenciarMarcadoresPage.ClicarEmConfirmarApagar();
    }

}
