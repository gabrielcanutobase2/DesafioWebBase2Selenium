package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.VerTarefasPage;
import net.serenitybdd.annotations.Step;

public class VerTarefasSteps {
    VerTarefasPage verTarefasPage;

    @Step("insiro o ID da tarefa no campo de pesquisa de filtro")
    public void insiroOIDDaTarefaNoCampoDePesquisaDeFiltro(String tarefaID) {
        verTarefasPage.PreencheCampoFiltro(tarefaID);
    }

    @Step("clico em Aplicar Filtro")
    public void clicoEmAplicarFiltro() {
        verTarefasPage.ClicaEmAplicarFiltro();
    }

    @Step("valido que não existe numero da tarefa no quadro")
    public boolean validoQueNaoExisteNumeroDaTarefaNoQuadro() {
        return verTarefasPage.retornaElementoVazio();
    }

    public boolean retornaIDTarefaTabela(String id) {
        return verTarefasPage.retornaValidacaoIDTabela(id);
    }

    public String retornIDTarefaVerDetalhes() {
        return verTarefasPage.retornaIDTarefaEmVerDetalhes();
    }

    public String retornaMensagemErro() {
        return verTarefasPage.retornaMensagemErroPesquisaTarefa();
    }

    @Step("insiro o ID da tarefa no campo de pesquisa")
    public void insiroOIDDaTarefaNoCampoDePesquisa(String tarefaID) {
        verTarefasPage.PreencheCampoPesquisaTarefa(tarefaID);
    }

    @Step("preencho o campo de Marcadores com um marcador")
    public void preenchoOCampoDeMarcadoresComUmMarcador(String tag) {
        verTarefasPage.PreencheCampoTag(tag);

    }


    @Step("clico em Aplicar Marcador")
    public void clicoEmAplicarMarcador() {
        verTarefasPage.ClicaEmAplicarTag();
    }


    @Step("Excluo o marcador criado")
    public void excluoOMarcadorCriado() {
        verTarefasPage.ClicaEmExcluirTag();
    }

    @Step("seleciono relacao {string}")
    public void selecionoRelacao(String tipoRelacao) {
        verTarefasPage.SelecionaTipoRelacao(tipoRelacao);
    }

    @Step("preencho campo de relacao com o ID da tarefa a ser relacionada")
    public void preenchoCampoDeRelacaoComOIDDaTarefaASerRelacionada(String tarefaID) {
        verTarefasPage.PreencheCampoRelacaoID(tarefaID);

    }

    @Step("clico em Adicionar")
    public void clicoEmAdicionar() {
        verTarefasPage.ClicaEmAdicionarRelacao();

    }

    @Step("preencho campo usuários monitorando tarefa  o nome do usuario desejado")
    public void preenchoCampoUsuariosMonitorandoTarefaONomeDoUsuarioDesejado(String usuario) {
        verTarefasPage.PreencheCampoUsuariosMonitorando(usuario);
    }

    @Step("clico em Adicionar Usuario")
    public void clicoEmAdicionarUsuario() {

        verTarefasPage.ClicaAdicionarUsuarioMonitoramento();

    }

    @Step("clico excluo o usuario adicionado")
    public void clicoExcluoOUsuarioAdicionado() {

        verTarefasPage.ClicaExcluirUsuarioMonitoramento();

    }
}
