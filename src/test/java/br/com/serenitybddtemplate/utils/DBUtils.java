package br.com.serenitybddtemplate.utils;

import java.sql.*;
import java.util.ArrayList;

public class DBUtils {

    public static ArrayList<String> retornaDadosQuery(String query) {
        ArrayList<String> arrayList = null;
        Connection connection = null;

        try {

            Statement stmt = null;
            connection = DriverManager.getConnection("jdbc:mysql://3306-cs-34e76a8d-e8ff-4b34-83dc-86fae98456da.cs-us-east1-rtep.cloudshell.dev/bugtracker?useTimezone=true&serverTimezone=UTC", "root", "root");

            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            if (!rs.isBeforeFirst()) {
                return null;
            }

            else{
                int numberOfColumns;
                ResultSetMetaData metadata=null;

                arrayList = new ArrayList<String>();
                metadata = rs.getMetaData();
                numberOfColumns = metadata.getColumnCount();

                while (rs.next()) {
                    int i = 1;
                    while(i <= numberOfColumns) {
                        arrayList.add(rs.getString(i++));
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static void executaQuery(String query) {
        Connection connection = null;

        try {
            Statement stmt = null;
            connection = DriverManager.getConnection("jdbc:mysql://3306-cs-34e76a8d-e8ff-4b34-83dc-86fae98456da.cs-us-east1-rtep.cloudshell.dev/bugtracker?useTimezone=true&serverTimezone=UTC", "root", "root");

            stmt = connection.createStatement();
            stmt.executeQuery(query);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void executaMudancas(String query) {
        Connection connection = null;

        try {
            Statement stmt = null;
            connection = DriverManager.getConnection("jdbc:mysql://3306-cs-34e76a8d-e8ff-4b34-83dc-86fae98456da.cs-us-east1-rtep.cloudshell.dev/bugtracker?useTimezone=true&serverTimezone=UTC", "root", "root");

            stmt = connection.createStatement();
            stmt.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
