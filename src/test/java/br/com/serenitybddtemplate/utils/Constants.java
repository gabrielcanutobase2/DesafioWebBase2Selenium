package br.com.serenitybddtemplate.utils;

public class Constants {

    public static String CATEGORY_ERROR = "Um campo necessário 'category' estava vazio. Por favor, verifique novamente suas entradas.";

    public static String TAREFA_PESQUISA_ERRO_LETRA_EM_CAMPO_NUMERICO = "Um número era esperado para bug_id.";

    public static String TAREFA_PESQUISA_ERRO_SIMBOLO_EM_CAMPO_NUMERICO = "Um número era esperado para bug_id.";

    public static String TAREFA_PESQUISA_ERRO_HIFEN_UNICO_EM_CAMPO_NUMERICO = "A tarefa 0 não encontrada.";
    public static String TAREFA_PESQUISA_ERRO_NAO_ENCONTRADA = "A tarefa %s não encontrada.";
    public static String TAREFA_RELACIONAR_ERRO_LETRA_EM_CAMPO_NUMERICO = "Valor inválido de 'target_issue_id'";
    public static String TAREFA_RELACIONAR_ERRO_HIFEN_UNICO_EM_CAMPO_NUMERICO = "Valor inválido de 'target_issue_id'";
    public static String TAREFA_RELACIONAR_ERRO_ID_NAO_ENCONTRADO = "A tarefa %s não encontrada.";
    public static String TAREFA_RELACIONAR_ERRO_ID_PROPRIO_ID = "Uma tarefa não pode se relacionar consigo mesma.";

    public static String TAREFA_RELACIONAR_ERRO_CAMPO_VAZIO = "Um parâmetro necessário para esta página (target_issue_id) não foi encontrado.";
    public static String TAREFA_MONITORAMENTO_USUARIO_NAO_ENCONTRADO = "Usuário de nome \"%s\" não foi encontrado.";

    public static String GERENCIAR_MARCADOR_ERRO_MARCADOR_JA_EXISTE = "Um marcador com esse nome já existe.";

    public static String LOGIN_ERROR = "Sua conta pode estar desativada ou bloqueada ou o nome de usuário e a senha que você digitou não estão corretos.";
    public static String CADASTRO_USUARIO_ERRO_EMAIL_INVALIDO = "E-mail inválido.";
    public static String CADASTRO_USUARIO_ERRO_NOME_INVALIDO = "O nome de usuário não é inválido. Nomes de usuário podem conter apenas letras, números, espaços, hífens, pontos, sinais de mais e sublinhados.";


    public static String LEVEL_VISUALIZADOR = "10";
    public static String LEVEL_RELATOR = "25";
    public static String LEVEL_ATUALIZADOR = "40";
    public static String LEVEL_DESENVOLVEDOR = "55";
    public static String LEVEL_GERENTE = "70";
    public static String LEVEL_ADMINISTRADOR = "90";


}
