package br.com.serenitybddtemplate.tests;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features",
        glue = {"br.com.serenitybddtemplate.hooks", "br.com.serenitybddtemplate.stepdefinitions"})
public class MantisBtTests {
}
