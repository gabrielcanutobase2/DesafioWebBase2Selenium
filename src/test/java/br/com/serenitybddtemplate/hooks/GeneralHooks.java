package br.com.serenitybddtemplate.hooks;

import br.com.serenitybddtemplate.dbsteps.BugTrackerDBSteps;
import br.com.serenitybddtemplate.dbsteps.ProjetosDBSteps;
import br.com.serenitybddtemplate.dbsteps.UsersDBSteps;
import br.com.serenitybddtemplate.steps.LoginSteps;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.serenitybdd.annotations.Steps;


public class GeneralHooks {
    @Steps
    LoginSteps loginSteps;


    @Before
    public void beforeScenario() {
        //código a ser executado antes de cada cenário

        UsersDBSteps usersDBSteps = new UsersDBSteps();
        ProjetosDBSteps projetosDBSteps = new ProjetosDBSteps();
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        projetosDBSteps.MassaProjetos();
        usersDBSteps.MassaUsuarios();
        bugTrackerDBSteps.CarregaMarcadores();
        loginSteps.abrirPaginaLogin();
    }

    @After
    public void afterScenario() {
        UsersDBSteps usersDBSteps = new UsersDBSteps();
        ProjetosDBSteps projetosDBSteps = new ProjetosDBSteps();
        BugTrackerDBSteps bugTrackerDBSteps = new BugTrackerDBSteps();
        projetosDBSteps.DeleteProjetosAfterDone();
        projetosDBSteps.DeleteCategoriasAfterDone();
        usersDBSteps.DeleteUsuariosAfterDone();
        bugTrackerDBSteps.DeleteMarcadoresTarefaAfterDone();
        bugTrackerDBSteps.DeleteRelacionamentoTarefaAfterDone();
        bugTrackerDBSteps.DeleteTarefasAfterDone();

        //código a ser executado depois de cada cenário
    }
}
