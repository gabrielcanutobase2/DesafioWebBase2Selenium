package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class GerenciarUsuarioPage extends PageBase {

    By campoCadastroUsuario = By.id("user-username");
    By campoCadastroNomeVerdadeiro = By.id("user-realname");
    By campoCadastroEmail = By.id("email-field");
    By botaoCriarNovaConta = By.xpath("//div[@id='manage-user-div']/div/a");
    By campoSelectNivelDeAcesso = By.id("user-access-level");
    By botaoCriarUsuario = By.xpath("//form[@id='manage-user-create-form']/div/div[3]/input");


    By botaoAtualizarUsuario = By.xpath("//form[@id='edit-user-form']/div/div[2]/div[2]/input");
    By botaoApagarUsuario = By.xpath("//form[@id='manage-user-delete-form']/fieldset/span/input");
    By botaoConfirmarApagarUsuario = By.xpath("//div[@id='main-container']/div[2]/div[2]/div/div/div[2]/form/input[4]");
    By campoAtualizarNivelDeAcesso = By.id("edit-access-level");
    By tableUsuarios = By.cssSelector("tbody");
    String userTag = "//a[text()='%s']";

    By mensagemErro = By.xpath("//p[2]");

    public GerenciarUsuarioPage(WebDriver driver) {
        super(driver);
    }


    public void ClicarEmCriarNovaConta() {
        click(botaoCriarNovaConta);

    }

    public void ClicaCheckBoxHabilitarDesabilitarUsuario() {
        javaScriptExecutor.executeScript("document.getElementById('edit-enabled').click()");

    }

    public void ClicaCheckBoxUsuarioProtegido() {
        javaScriptExecutor.executeScript("document.getElementById('edit-protected').click()");

    }

    public void CriarNovoUsuario(String usuario, String nomeReal, String email, String nivelDeAcesso) {
        sendKeys(campoCadastroUsuario, usuario);
        sendKeys(campoCadastroNomeVerdadeiro, nomeReal);
        sendKeys(campoCadastroEmail, email);
        comboBoxSelectByVisibleText(campoSelectNivelDeAcesso, nivelDeAcesso);

    }

    public void IncluirUsuario() {
        click(botaoCriarUsuario);

    }

    public void ApagarUsuario() {
        click(botaoApagarUsuario);

    }

    public void ConfirmarApagarUsuario() {
        click(botaoConfirmarApagarUsuario);

    }

    public void AtualizaNivelDeAcesso(String nivelDeAcessoNovo) {
        comboBoxSelectByVisibleText(campoAtualizarNivelDeAcesso, nivelDeAcessoNovo);
    }

    public void AtualizarUsuario() {
        click(botaoAtualizarUsuario);

    }

    public String retornaNomeTabela(String nomeUsuario) {

        List<WebElementFacade> linhas = find(tableUsuarios).thenFindAll("tr");
        for (WebElementFacade linha : linhas) {
            boolean encontrou = linha.findElements(By.cssSelector("tr>td")).stream().anyMatch(col -> col.getText().equals(nomeUsuario));
            if (encontrou) {
                return nomeUsuario;

            }
        }
        return "";
    }

    public void ClicaUsuarioNaTabela(String nomeUsuario) {

        List<WebElementFacade> linhas = find(tableUsuarios).thenFindAll("tr");
        for (WebElementFacade linha : linhas) {
            boolean encontrou = linha.findElements(By.cssSelector("tr>td")).stream().anyMatch(col -> col.getText().equals(nomeUsuario));
            if (encontrou) {
                click(By.xpath(String.format(userTag, nomeUsuario)));
                break;
            }
        }
    }

    public String retornaMensagemErro() {
        return waitForElement(mensagemErro).getText();
    }

}
