package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class GerenciarProjetoPage extends PageBase {

    By campoNomeProjeto = By.id("project-name");
    By campoSelectStatusProjeto = By.id("project-status");
    By campoSelectVisibilidade = By.id("project-view-state");

    By campoDescricao = By.id("project-description");
    By botaoCriarNovoProjeto = By.cssSelector(".widget-toolbox > .form-inline .btn");

    By botaoAdicionarProjeto = By.xpath("//input[@type='submit']");

    By tableProjetos = By.cssSelector("div.page-content > div > div > div.widget-box.widget-color-blue2 > div.widget-body > div > div.table-responsive > table > tbody");


    By tableCategorias = By.cssSelector("#categories > div > div.widget-body > div > div > table > tbody");
    By campoNomeCategoria = By.xpath("//*[@id='categories']/div/div[2]/form/div/input[3]");
    By botaoAdicionarCategoria = By.cssSelector(".btn:nth-child(4)");

    public GerenciarProjetoPage(WebDriver driver) {
        super(driver);
    }


    public void ClicarEmCriarNovoProjeto() {
        click(botaoCriarNovoProjeto);

    }

    public void CriarNovoProjeto(String projeto, String estado, String visibilidade, String descricao) {
        sendKeys(campoNomeProjeto, projeto);
        comboBoxSelectByVisibleText(campoSelectStatusProjeto, estado);
        comboBoxSelectByVisibleText(campoSelectVisibilidade, visibilidade);
        sendKeys(campoDescricao, descricao);

    }

    public void AdicionarNovaCategoria(String categoria) {
        sendKeys(campoNomeCategoria, categoria);

    }

    public void ClicarEmAdicionarNovaCategoria() {
        click(botaoAdicionarCategoria);

    }

    public void ClicarEmAdicionarProjeto() {
        click(botaoAdicionarProjeto);

    }

    public boolean retornaValidacaoNomeTabela(String nome) {

        List<WebElementFacade> linhas = find(tableProjetos).thenFindAll("tr");
        for (WebElementFacade linha : linhas) {
            boolean encontrou = linha.findElements(By.cssSelector("tr>td")).stream().anyMatch(col -> col.getText().equals(nome));
            if (encontrou) {
                return true;

            }
        }
        return false;
    }

}
