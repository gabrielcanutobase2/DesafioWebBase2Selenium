package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class GerenciarMarcadoresPage extends PageBase {

    By campoCadastroMarcador = By.id("tag-name");
    By campoDescricaoMarcador = By.id("tag-description");

    By botaoCriarNovoMarcador = By.xpath("//a[contains(@href, '#tagcreate')]");
    By botaoCriarMarcador = By.name("config_set");
    By botaoAtualizarMarcador = By.cssSelector(".btn-white:nth-child(2)");
    By botaoConfirmarAtualizarMarcador = By.cssSelector(".widget-toolbox:nth-child(3) > .btn");
    By botaoApagarMarcador = By.xpath("//input[@value='Apagar Marcador']");
    By botaoConfirmarApagarMarcador = By.cssSelector(".btn-white");
    By tableMarcadores = By.cssSelector("div.widget-main.no-padding > div > table > tbody");
    String marcadorTag = "//a[text()='%s']";
    By mensagemErro = By.xpath("//p[2]");

    public GerenciarMarcadoresPage(WebDriver driver) {
        super(driver);
    }


    public void ClicarEmCriarNovoMarcador() {
        click(botaoCriarNovoMarcador);

    }

    public void ClicarEmApagar() {
        click(botaoApagarMarcador);

    }

    public void ClicarEmConfirmarApagar() {
        click(botaoConfirmarApagarMarcador);

    }

    public void PreencheDadosNovoMarcador(String nomeMarcador, String descricaoMarcador) {
        sendKeys(campoCadastroMarcador, nomeMarcador);
        sendKeys(campoDescricaoMarcador, descricaoMarcador);

    }

    public void IncluirMarcador() {
        click(botaoCriarMarcador);

    }

    public void AtualizarNomeMarcador(String novoNome) {
        clear(campoCadastroMarcador);
        sendKeys(campoCadastroMarcador, novoNome);
    }

    public void AtualizarMarcador() {
        click(botaoAtualizarMarcador);

    }

    public void ConfirmarAtualizarMarcador() {
        click(botaoConfirmarAtualizarMarcador);

    }

    public boolean retornaNomeTabela(String nomeMarcador) {

        List<WebElementFacade> linhas = find(tableMarcadores).thenFindAll("tr");
        for (WebElementFacade linha : linhas) {
            boolean encontrou = linha.findElements(By.cssSelector("tr>td")).stream().anyMatch(col -> col.getText().equals(nomeMarcador));
            if (encontrou) {
                return true;

            }
        }
        return false;
    }

    public void SelecionaMarcadorNaTabela(String nomeMarcador) {

        List<WebElementFacade> linhas = find(tableMarcadores).thenFindAll("tr");
        for (WebElementFacade linha : linhas) {
            boolean encontrou = linha.findElements(By.cssSelector("tr>td")).stream().anyMatch(col -> col.getText().equals(nomeMarcador));
            if (encontrou) {
                click(By.xpath(String.format(marcadorTag, nomeMarcador)));
                break;
            }
        }
    }

    public String retornaMensagemErro() {
        return waitForElement(mensagemErro).getText();
    }

}
