package br.com.serenitybddtemplate.pages;


import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CadastroPage extends PageBase {

    //Constructor
    public CadastroPage(WebDriver driver) {
        super(driver);
    }

    //Mapping
    By campoCadastroUsuario = By.id("username");
    By campoCadastroEmail = By.id("email-field");

    By mensagemErroTextArea = By.xpath("/html/body/div[2]/font");


    //Actions
    public void preencheCampoCadastroUsuario(String usuario) {
        sendKeys(campoCadastroUsuario, usuario);
    }

    public void preencherCampoCadastroEmail(String email) {
        sendKeys(campoCadastroEmail, email);
    }


    public String retornaMensagemDeErro() {
        return getText(mensagemErroTextArea);
    }
}
