package br.com.serenitybddtemplate.pages;


import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends PageBase {

    //Constructor
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    //Mapping
    By usernameField = By.id("username");
    By passwordField = By.id("password");
    By submitUserButton = By.xpath("//form[@id='login-form']/fieldset/input[2]");
    By submitButton = By.xpath("//form[@id='login-form']/fieldset/input[3]");
    By botaoCadastrar = By.xpath("//*[contains(@class,'back-to-login-link')]");
    By mensagemErroTextArea = By.cssSelector("div.alert.alert-danger > p");


    //Actions
    public void preencherUsuario(String usuario) {
        sendKeys(usernameField, usuario);
    }

    public void preencherUsuarioJavasCript(String usuario) {
        sendKeysJavaScript(usernameField, usuario);
    }

    public void preencherSenha(String senha) {
        sendKeys(passwordField, senha);
    }

    public void preencherSenhaJavaScript(String senha) {
        sendKeysJavaScript(passwordField, senha);
    }

    public void clicarEmEntrarComUsuario() {
        click(submitUserButton);
    }

    public void clicarEmLogar() {
        click(submitButton);
    }

    public void clicarEmEntrarComUsuarioJavascript() {
        clickJavaScript(submitUserButton);
    }

    public void clicarEmLogarJavascript() {
        clickJavaScript(submitButton);
    }

    public void clicarEmCadastrar() {
        click(botaoCadastrar);
    }

    public String retornaMensagemDeErro() {
        return getText(mensagemErroTextArea);
    }
}
