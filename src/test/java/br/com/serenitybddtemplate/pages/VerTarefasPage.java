package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class VerTarefasPage extends PageBase {

    public VerTarefasPage(WebDriver driver) {
        super(driver);
    }

    //region Fields
    By campoTextoFiltro = By.id("filter-search-txt");
    By campoPesquisaTarefa = By.name("bug_id");
    By verDetalhesTarefaCampoTag = By.id("tag_string");
    By verDetalhesTarefaCampoTarefaRelacionada = By.name("dest_bug_id");
    By verDetalhesTarefaCampoNomeUsuario = By.id("bug_monitor_list_username");

    //endregion

    //region Inputs,Buttons e Boxes
    By verDetalhesTarefaBotaoAdicionarRelacao = By.name("add_relationship");
    By botaoAplicarFiltro = By.xpath("//input[@name='filter_submit']");
    By verDetalhesTarefaBotaoAplicarTag = By.cssSelector(".btn:nth-child(7)");
    By verDetalhesTarefaBotaoExcluirTag = By.cssSelector("tbody > tr:nth-child(12) > td > a:nth-child(2)");
    By verDetalhesTarefaBotaoExcluirUsuarioMonitoramento = By.cssSelector("tbody > tr > td > div:nth-child(1) > a.btn.btn-xs.btn-primary.btn-white.btn-round");

    By verDetalhesTarefaBotaoAdicionarUsuarioMonitoramento = By.cssSelector("#monitoring > div.widget-body > div > div > table > tbody > tr > td > form > input.btn.btn-primary.btn-sm.btn-white.btn-round");
    //endregion

    //region Selects e ComboBoxes
    By verDetalhesTarefaSelectRelacao = By.name("rel_type");
    //endregion

    //region Texts
    By mensagemErro = By.xpath("//p[2]");
    By verDetalhesIDTarefa = By.xpath("//*[@class='bug-id']");
    //endregion

    //region Tables
    By tabelaTarefas = By.cssSelector("#buglist > tbody");
    //endregion

    public void SelecionaTipoRelacao(String tipoRelacao) {
        comboBoxSelectByVisibleText(verDetalhesTarefaSelectRelacao, tipoRelacao);
    }

    public void PreencheCampoUsuariosMonitorando(String usuario) {
        sendKeys(verDetalhesTarefaCampoNomeUsuario, usuario);
    }

    public void ClicaExcluirUsuarioMonitoramento() {

        click(verDetalhesTarefaBotaoExcluirUsuarioMonitoramento);
    }

    public void ClicaAdicionarUsuarioMonitoramento() {

        click(verDetalhesTarefaBotaoAdicionarUsuarioMonitoramento);
    }

    public void PreencheCampoRelacaoID(String tarefaID) {
        sendKeys(verDetalhesTarefaCampoTarefaRelacionada, tarefaID);
    }

    public void ClicaEmAdicionarRelacao() {
        click(verDetalhesTarefaBotaoAdicionarRelacao);
    }

    public void ClicaEmAplicarTag() {
        click(verDetalhesTarefaBotaoAplicarTag);
    }

    public void ClicaEmExcluirTag() {
        click(verDetalhesTarefaBotaoExcluirTag);
    }

    public void PreencheCampoTag(String tag) {
        sendKeys(verDetalhesTarefaCampoTag, tag);

    }

    public void PreencheCampoFiltro(String tarefaID) {
        sendKeys(campoTextoFiltro, tarefaID);

    }

    public void PreencheCampoPesquisaTarefa(String tarefaID) {
        find(campoPesquisaTarefa).then().typeAndEnter(tarefaID);
    }

    public void ClicaEmAplicarFiltro() {
        click(botaoAplicarFiltro);
    }

    public String retornaIDTarefaEmVerDetalhes() {
        return getText(verDetalhesIDTarefa);
    }

    public boolean retornaElementoVazio() {
        int rows = find(tabelaTarefas).thenFindAll("tr").size();
        if (rows > 0) {
            return false;
        } else if (rows == 0) {

            return true;
        }

        return false;

    }

    public boolean retornaValidacaoIDTabela(String id) {

        List<WebElementFacade> linhas = find(tabelaTarefas).thenFindAll("tr");
        for (WebElementFacade linha : linhas) {
            boolean encontrou = linha.findElements(By.cssSelector("tr>td")).stream().anyMatch(col -> col.getText().equals(id));
            if (encontrou) {
                return true;

            }
        }
        return false;
    }


    public String retornaMensagemErroPesquisaTarefa() {
        return waitForElement(mensagemErro).getText();
    }

}
