package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BugTrackerPage extends PageBase {

    public BugTrackerPage(WebDriver driver) {
        super(driver);
    }


    //region Fields
    By campoSelectCategoria = By.id("category_id");
    By campoSelectFrequencia = By.id("reproducibility");
    By campoSelectGravidade = By.id("severity");
    By campoSelectPrioridade = By.id("priority");
    By campoPerfilDispositivo = By.id("profile_open");
    By campoPerfilPlataforma = By.id("platform");
    By campoPerfilSO = By.id("os");
    By campoPerfilVersaoSO = By.id("os_build");
    By campoSelectAtribuir = By.id("handler_id");
    By campoResumo = By.id("summary");
    By campoDescricao = By.id("description");
    By campoPassosAReproduzir = By.id("steps_to_reproduce");
    By campoInformacoesAdicionais = By.id("additional_info");
    By campoMarcadores = By.id("tag_string");
    //endregion

    //region Dropzones
    By dropzoneEnviarArquivos = By.cssSelector("div.dropzone.center.dz-clickable");
    //endregion

    //region Inputs,Buttons e Boxes

    By radioVisibilidadePublico = By.xpath("//*[@id='report_bug_form']/div/div[2]//tr[13]/td/label[1]/input");
    By radioVisibilidadePrivado = By.xpath("//*[@id='report_bug_form']/div/div[2]//tr[13]/td/label[2]/input");
    By checkBoxContinuarAdicionando = By.id("report_stay");
    By botaoCriarNovaTarefa = By.xpath("//input[@type='submit']");
    //endregion

    //region Textos
    By mensagemErroCategory = By.cssSelector("div.alert.alert-danger > p:nth-child(2)");
    By textoBugID = By.xpath("//td[@class = 'bug-id']");
    //endregion


    public void EnviarEvidencia(String path) {
        sendKeys(dropzoneEnviarArquivos, path);
    }

    public void ClickRadioPublico() {
        click(radioVisibilidadePublico);
    }

    public void ClickRadioPrivado() {
        click(radioVisibilidadePrivado);
    }

    public void ClickCheckboxContinuarAdicionando() {
        click(checkBoxContinuarAdicionando);
    }

    public void ClickBotaoCriarNovaTarefa() {
        click(botaoCriarNovaTarefa);
    }

    public void SelecionoAOpcaoDeCategoria(String opcao) {
        comboBoxSelectByVisibleText(campoSelectCategoria, opcao);

    }

    public void SelecionoAOpcaoDeFrequencia(String opcao) {
        comboBoxSelectByVisibleText(campoSelectFrequencia, opcao);

    }

    public void SelecionoAOpcaoDePrioridade(String opcao) {
        comboBoxSelectByVisibleText(campoSelectPrioridade, opcao);

    }

    public void SelecionoAOpcaoDeGravidade(String opcao) {
        comboBoxSelectByVisibleText(campoSelectGravidade, opcao);

    }

    public void SelecionoAOpcaoDeAtribuir(String opcao) {
        comboBoxSelectByVisibleText(campoSelectAtribuir, opcao);

    }

    public void InsiroInformacoesNoCampoPerfilDispositivo(String informacoes) {
        sendKeys(campoPerfilDispositivo, informacoes);

    }

    public void InsiroInformacoesNoCampoPerfilPlataforma(String informacoes) {
        sendKeys(campoPerfilPlataforma, informacoes);

    }

    public void InsiroInformacoesNoCampoPerfilSO(String informacoes) {
        sendKeys(campoPerfilSO, informacoes);

    }

    public void InsiroInformacoesNoCampoPerfilVersaoSO(String informacoes) {
        sendKeys(campoPerfilVersaoSO, informacoes);

    }

    public void InsiroInformacoesNoCampoResumo(String informacoes) {
        sendKeys(campoResumo, informacoes);

    }

    public void InsiroInformacoesNoCampoDescricao(String informacoes) {
        sendKeys(campoDescricao, informacoes);

    }

    public void InsiroInformacoesNoCampoPassosAReproduzir(String informacoes) {
        sendKeys(campoPassosAReproduzir, informacoes);

    }

    public void InsiroInformacoesNoCampoInformacoesAdicionais(String informacoes) {
        sendKeys(campoInformacoesAdicionais, informacoes);

    }

    public void InsiroInformacoesNoCampoMarcadores(String informacoes) {
        sendKeys(campoMarcadores, informacoes);

    }

    public String retornaErroCategoriaNaoInserida() {
        return waitForElement(mensagemErroCategory).getText();
    }

    public String retornaBugIDTexto() {

        waitForElement(textoBugID);
        return getText(textoBugID);

    }
}
