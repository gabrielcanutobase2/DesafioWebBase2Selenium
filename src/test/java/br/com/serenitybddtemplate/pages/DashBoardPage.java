package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class DashBoardPage extends PageBase {
    public DashBoardPage(WebDriver driver) {
        super(driver);
    }

    By dashSideMenuMinhaVisao = By.xpath("//a[contains(@href, '/my_view_page.php')]");
    By dashSideMenuVerTarefas = By.xpath("//a[contains(@href, '/view_all_bug_page.php')]");
    By dashSideMenuCriarTarefa = By.xpath("//a[contains(@href, '/bug_report_page.php')]");
    By dashSideMenuRegistroDeMudancas = By.xpath("//a[contains(@href, '/changelog_page.php')]");
    By dashSideMenuPlanejamento = By.xpath("//a[contains(@href, '/roadmap_page.php')]");
    By dashSideMenuResumo = By.xpath("//a[contains(@href, '/summary_page.php')]");
    By dashSideMenuGerenciar = By.xpath("//a[contains(@href,'/manage_overview_page.php')]");
    By manageButton = By.xpath("//button[@class='btn btn-crowdtest mr-1']");
    By menuListButton = By.xpath("//button[@class='toggle-menu-btn open-menu-btn']");

    By projectRow = By.xpath("//mat-row[@class ='mat-row ng-star-inserted']");
    By nomeUsuarioTelaLogin = By.xpath("//span[@class = 'user-info']");

    By botaoSelecionarProjeto = By.xpath("//*[@id='select-project-form']/div/div[2]/div[2]/input");

    public void IrParaDashboard() {
        click(manageButton);
    }

    public void AbrirMenu() {
        click(menuListButton);

    }

    public void ClicarEmMinhaVisao() {
        click(dashSideMenuMinhaVisao);
    }

    public void ClicarEmVerTarefas() {
        click(dashSideMenuVerTarefas);
    }

    public void ClicarEmCriarTarefa() {
        click(dashSideMenuCriarTarefa);
    }

    public void ClicarEmRegistroMudancas() {
        click(dashSideMenuRegistroDeMudancas);
    }

    public void ClicarEmPlanejamento() {
        click(dashSideMenuPlanejamento);
    }

    public void ClicarEmResumo() {
        click(dashSideMenuResumo);
    }

    public void ClicarEmGerenciar() {
        click(dashSideMenuGerenciar);
    }

    public void SelecionarProjeto() {
        click(projectRow);
    }

    public void ClicarBotaoSelecionarProjeto() {
        click(botaoSelecionarProjeto);

    }

    public String retornaNomeUsuarioEmTela() {
        return getText(nomeUsuarioTelaLogin);
    }
}
