package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GerenciarPage extends PageBase {

    By tabGerenciarUsuarios = By.xpath("//*[@href='/manage_user_page.php']");
    By tabGerenciarProjetos = By.xpath("//*[@href='/manage_proj_page.php']");
    By tabGerenciarMarcadores = By.xpath("//*[@href='/manage_tags_page.php']");
    By tabGerenciarCamposPersonalizados = By.xpath("//*[@href='/manage_custom_field_page.php']");
    By tabGerenciarPerfisGlobais = By.xpath("//*[@href='/manage_prof_menu_page.php']");
    By tabGerenciarPlugins = By.xpath("//*[@href='/manage_plugin_page.php']");
    By tabGerenciarConfiguracao = By.xpath("//*[@href='/adm_permissions_report.php']");

    public GerenciarPage(WebDriver driver) {
        super(driver);
    }


    public void ClicarEmGerenciarUsuarios() {

        click(tabGerenciarUsuarios);
    }

    public void ClicarEmGerenciarProjetos() {

        click(tabGerenciarProjetos);
    }

    public void ClicarEmGerenciarMarcadores() {

        click(tabGerenciarMarcadores);
    }

    public void ClicarEmGerenciarCamposPersonalizados() {

        click(tabGerenciarCamposPersonalizados);
    }

    public void ClicarEmGerenciarPerfisGlobais() {

        click(tabGerenciarPerfisGlobais);
    }

    public void ClicarEmGerenciarPlugins() {

        click(tabGerenciarPlugins);
    }

    public void ClicarEmGerenciarConfiguracao() {

        click(tabGerenciarConfiguracao);
    }
}
