#language: pt
@Login @mantisbt
Funcionalidade: Login

  @EfetuaLoginComSucesso
  Esquema do Cenario: Efetuar Login com sucesso
    Dado que informo o nome de usuario "<USUARIO>"
    E clico em Entrar com usuário
    E informo a senha "<SENHA>"
    Quando acesso a minha conta
    Entao o usuário "<USUARIO>" deve ser autenticado com sucesso
    Exemplos:
      | USUARIO       | SENHA        |
      | administrator | mantispassbt |
      | user1         | mantispassbt |
      | user2         | mantispassbt |
      | user3         | mantispassbt |

  @EfetuaLoginComSucessoJAVASCRIPT
  Esquema do Cenario: Efetuar Login com sucesso com JAVASCRIPT
    Dado que informo o nome de usuario "<USUARIO>" por Javascript
    E clico em Entrar por Javascript
    E informo a senha "<SENHA>" por Javascript
    Quando acesso a minha conta
    Entao o usuário "<USUARIO>" deve ser autenticado com sucesso
    Exemplos:
      | USUARIO       | SENHA        |
      | administrator | mantispassbt |
      | user1         | mantispassbt |
      | user2         | mantispassbt |
      | user3         | mantispassbt |

  @ValidaErroLoginUsuarioInvalido
  Esquema do Cenario: Efetuar Login usuário invalido
    Dado que informo o nome de usuario "<USUARIO>"
    E clico em Entrar com usuário
    E informo a senha "<SENHA>"
    Quando acesso a minha conta
    Entao o sistema retorna uma mensagem de erro de login
    Exemplos:
      | USUARIO        | SENHA        |
      | adenistrato    | mantispassbt |
      | admnistrator   | mantispassbt |
      | adimnistrator  | mantispassbt |
      | addministrator | mantispassbt |
      | administrat    | mantispassbt |

  @ValidaErroLoginSenhaInvalida
  Esquema do Cenario: Efetuar Login Senha invalida
    Dado que informo o nome de usuario "<USUARIO>"
    E clico em Entrar com usuário
    E informo a senha "<SENHA>"
    Quando acesso a minha conta
    Entao o sistema retorna uma mensagem de erro de login
    Exemplos:
      | USUARIO       | SENHA                 |
      | administrator | mantispesbt           |
      | administrator | mantispazbt           |
      | administrator | matispassbt           |
      | administrator | m4ntispassbt          |
      | administrator | 12nhj3u123n1j231b2j3b |

  @ValidaErroLoginUsuarioESenhaInvalida
  Esquema do Cenario: Efetuar Login Usuario e Senha invalida
    Dado que informo o nome de usuario "<USUARIO>"
    E clico em Entrar com usuário
    E informo a senha "<SENHA>"
    Quando acesso a minha conta
    Entao o sistema retorna uma mensagem de erro de login
    Exemplos:
      | USUARIO       | SENHA                 |
      | administratus | mantispesbt           |
      | administrat0r | mantispazbt           |
      | administrator | matispassbt           |
      | dministrator  | m4ntispassbt          |
      | aministrator  | 12nhj3u123n1j231b2j3b |

  @ValidaErroUsuarioDesativado
  Esquema do Cenario: Efetuar Login Usuario Desativado
    Dado que tenho um usario inativo no banco
    E que informo o nome de usuario "<USUARIO>"
    E clico em Entrar com usuário
    E informo a senha "<SENHA>"
    Quando acesso a minha conta
    Entao o sistema retorna uma mensagem de erro de login
    Exemplos:
      | USUARIO            | SENHA                 |
      | usuario.desativado | senhaDeUsuarioInativo |