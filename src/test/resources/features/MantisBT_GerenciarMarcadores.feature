#language: pt
@GerenciarMarcadores @mantisbt
Funcionalidade: Gerenciar Marcadores

  Contexto:
    Dado que acesso o site como 'Administrador'


  @CriarUmNovoMarcador
  Cenario: Cria um novo marcador
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Marcadores
    E clico em Criar Novo Marcador
    E insiro o nome e uma descricao para o marcador
    E clico em Criar Marcador
    Entao valido no banco que o marcador foi criado com sucesso


  @EditarNomeMarcadorComSucesso
  Cenario: Edita Nome de um Marcador com sucesso
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Marcadores
    E seleciono um marcador da tabela
    E altero o nome do marcador para um novo
    E clico em Atualizar Marcador
    Entao valido no banco que o marcador mudou de nome

  @EditarNomeMarcadorParaNomeExistente
  Cenario: Edita Nome de um Marcador para o nome de outro Marcador Existente
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Marcadores
    E seleciono um marcador da tabela
    E altero o nome do marcador para o de outro marcador existente
    E clico em Atualizar Marcador
    Entao valido mensagem de erro nome do marcador nao pode ser igual a de existente

  @EditarDescricaoMarcador
  Cenario: Atualiza um usuario para Protegido
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E marco a caixa de Protegido
    E clico em Atualizar Usuário
    Entao valido no banco de dados se o usuario tem o status de Protegido

  @ExcluiMarcadorComSucesso
  Cenario: Exlui um Marcador na aba de Gerenciar com sucesso
    Dado que possuo um marcador existente
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Marcadores
    E seleciono um marcador criado na tabela
    E clico em Apagar
    E clico novamente em Apagar para confirmar a exclusao
    Entao verifico pelo nome no banco de dados se o marcador foi excluido com sucesso