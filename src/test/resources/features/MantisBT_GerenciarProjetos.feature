#language: pt
@GerenciarProjetos @mantisbt
Funcionalidade: Gerenciar Projetos

  Contexto:
    Dado que acesso o site como 'Administrador'

  @AdicionaProjetoComSucesso
  Esquema do Cenario: Adicionar um Projeto com sucesso
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Projetos
    E clico em Criar Novo Projeto
    E preencho os campos do projeto com um "<NOMEPROJETO>","<STATUS>","<VISIBILIDADE>","<DESCRICAO>"
    Entao valido que o projeto com nome "<NOMEPROJETO>" foi cadastrado com sucesso
    Exemplos:
      | NOMEPROJETO | STATUS          | VISIBILIDADE | DESCRICAO                                                     |
      | Projeto1    | desenvolvimento | público      | Teste de descricao 1 com uma alta quantidade de palavras aqui |
      | Projeto2    | release         | privado      | Teste de descricao 2 com uma alta quantidade de palavras aqui |

  @AdicionCategoriaComSucesso
  Esquema do Cenario: Adicionar uma Categoria com sucesso
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Projetos
    E preencho o campo de Categoria com "<NOMECATEGORIA>"
    E clico em Adicionar Categoria
    Entao valido que a categoria com nome "<NOMECATEGORIA>" foi cadastrada com sucesso
    Exemplos:
      | NOMECATEGORIA |
      | Projeto1      |
      | Projeto2      |

