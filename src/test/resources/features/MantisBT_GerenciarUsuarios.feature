#language: pt
@Gerenciar @mantisbt
Funcionalidade: Gerenciar Usuarios

  Contexto:
    Dado que acesso o site como 'Administrador'

  @EfetuaCadastroUsuarioComSucesso
  Esquema do Cenario: Efetuar Cadastro de novos usuarios
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E clico em Criar Nova Conta
    E preencho os campos com um "<NOME>","<NOMEVERDADEIRO>","<EMAIL>","<NIVELACESSO>"
    Entao valido que o usuário com nome "<NOME>" foi cadastrado com sucesso
    Exemplos:
      | NOME    | NOMEVERDADEIRO          | EMAIL            | NIVELACESSO |
      | Gabriel | Gabriel Teste Sobrenome | gabriel@teste.br | atualizador |

  @EfetuaCadastroUsuarioEmailInvalido
  Esquema do Cenario: Valida erro ao inserir Email Invalido
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E clico em Criar Nova Conta
    E preencho os campos com um "<NOME>","<NOMEVERDADEIRO>","<EMAIL>","<NIVELACESSO>"
    Entao valido mensagem de erro que o email inserido é invalido
    Exemplos:
      | NOME    | NOMEVERDADEIRO          | EMAIL               | NIVELACESSO |
      | Gabriel | Gabriel Teste Sobrenome | gabrielEmail.com.br | atualizador |

  @EfetuaCadastroUsuarioNomeUsuarioVazio
  Esquema do Cenario: Valida erro ao deixar o campo do nome de usuário vazio
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E clico em Criar Nova Conta
    E preencho os campos com um "<NOME>","<NOMEVERDADEIRO>","<EMAIL>","<NIVELACESSO>"
    Entao valido mensagem de erro que o nome de usuário é invalido
    Exemplos:
      | NOME | NOMEVERDADEIRO          | EMAIL               | NIVELACESSO |
      |      | Gabriel Teste Sobrenome | gabrielEmail.com.br | atualizador |

  @EfetuaCadastroUsuarioNomeInvalidoPorSimbolos
  Esquema do Cenario: Valida erro inserindo simbolos no campo do nome
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E clico em Criar Nova Conta
    E preencho os campos com um "<NOME>","<NOMEVERDADEIRO>","<EMAIL>","<NIVELACESSO>"
    Entao valido mensagem de erro que o nome de usuário é invalido
    Exemplos:
      | NOME | NOMEVERDADEIRO          | EMAIL               | NIVELACESSO |
      | !%#$ | Gabriel Teste Sobrenome | gabrielEmail.com.br | atualizador |


  @ApagarUmUsuário
  Cenario: Apaga um usuario existente
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E clico em Apagar Usuario
    E confirmo a exclusão do usuário
    Entao valido no banco de dados que o usuário foi excluido com sucesso

  @AtualizaNivelAcessoDesenvolvedor
  Cenario: Atualiza nivel de acesso do usuario para Desenvolvedor
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E mudo o nivel dele para Desenvolvedor
    E clico em Atualizar Usuário
    Entao valido no banco de dados o nivel de acesso do usuario igual a Desenvolvedor

  @AtualizaNivelAcessoAtualizador
  Cenario: Atualiza nivel de acesso do usuario para Atualizador
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E mudo o nivel dele para Atualizador
    E clico em Atualizar Usuário
    Entao valido no banco de dados o nivel de acesso do usuario igual a Atualizador

  @AtualizaNivelAcessoRelator
  Cenario: Atualiza nivel de acesso do usuario para Relator
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E mudo o nivel dele para Relator
    E clico em Atualizar Usuário
    Entao valido no banco de dados o nivel de acesso do usuario igual a Relator

  @AtualizaNivelAcessoVizualizador
  Cenario: Atualiza nivel de acesso do usuario para Vizualizador
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E mudo o nivel dele para Visualizador
    E clico em Atualizar Usuário
    Entao valido no banco de dados o nivel de acesso do usuario igual a Visualizador

  @AtualizaNivelAcessoAdministrador
  Cenario: Atualiza nivel de acesso do usuario para Administrador
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E mudo o nivel dele para Administrador
    E clico em Atualizar Usuário
    Entao valido no banco de dados o nivel de acesso do usuario igual a Administrador

  @AtualizaNivelAcessoGerente
  Cenario: Atualiza nivel de acesso do usuario para Gerente
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E mudo o nivel dele para Gerente
    E clico em Atualizar Usuário
    Entao valido no banco de dados o nivel de acesso do usuario igual a Gerente


  @AtualizaAtividadeUsuarioHabilitado
  Cenario: Atualiza Atividade do usuario para Desabilitado
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E desmarco a caixa Habilitado
    E clico em Atualizar Usuário
    Entao valido no banco de dados se o usuario foi desabilitado

  @AtualizaUsuarioParaProtegido
  Cenario: Atualiza um usuario para Protegido
    Quando acesso a tela Gerenciar
    E clico em Gerenciar Usuarios
    E seleciono um usuario da tabela
    E marco a caixa de Protegido
    E clico em Atualizar Usuário
    Entao valido no banco de dados se o usuario tem o status de Protegido