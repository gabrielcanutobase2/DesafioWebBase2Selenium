#language: pt
@Bugtracker @mantisbt
Funcionalidade: Adicionar, Editar e Excluir uma Tarefa

  Contexto:
    Dado que acesso o site como 'Administrador'

  @TarefaComSucesso
  Cenario: Criar uma Tarefa com sucesso
    Quando acessoa a tela Criar Tarefa
    E preencho os campos obrigatórios do Relatório
    E clico em Criar Nova tarefa
    Entao verifico se tarefa foi criada com sucesso pelo banco de dados


  @TarefaSemCategoria
  Cenario: Valida mensagem erro de uma Tarefa criada sem uma categoria
    Dado que tenho Categorias criadas
    Quando acessoa a tela Criar Tarefa
    E preencho os campos obrigatórios do Relatório exceto CATEGORIA
    E clico em Criar Nova tarefa
    Entao valido mensagem de erro ao tentar inserir tarefa sem uma categoria


  @VerificaTarefaEmQuadro
  Cenario: Verifica se tarefa foi criada no quadro do 'Ver Tarefa'
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    Entao valido numero de registro da tarefa no quadro


  @VerificaTarefaPorFiltroSucesso
  Cenario: Verifica se tarefa é retornada por filtro selecionado
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E insiro o ID da tarefa no campo de filtro
    E clico em Aplicar Filtro
    Entao valido numero de registro da tarefa no quadro


  @VerificaTarefaPorFiltroNumeroInvalido
  Cenario: Valida pesquisa por filtro com um numero inexistente
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E insiro um ID INVALIDO da tarefa no campo de filtro
    E clico em Aplicar Filtro
    Entao valido que não existe numero da tarefa no quadro

  @VerificaTarefaCampoPesquisaSucesso
  Cenario: Verifica tarefa através do campo de Pesquisa
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E insiro o ID da tarefa no campo de pesquisa
    Entao verifico se a tarefa mostrada na tela tem o mesmo ID da tabela criada


  @VerificaTarefaCampoPesquisaNumeroInvalido
  Cenario: Valida mensagem de erro ao inserir um numero invalido no campo Pesquisa
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E insiro um ID INVALIDO da tarefa no campo de pesquisa
    Entao valido mensagem de erro numero de tarefa não encontrado

  @VerificaTarefaCampoPesquisaLetraNoCampoPesquisa
  Cenario: Valida mensagem de erro ao inserir uma letra no campo Pesquisa
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E insiro um caractere no campo de pesquisa
    Entao valido mensagem de erro caracter inserido não esperado

  @VerificaTarefaCampoPesquisaHifenNoCampoPesquisa
  Cenario: Valida mensagem de erro ao inserir um hifen no campo Pesquisa
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E insiro um hifen no campo de pesquisa
    Entao valido mensagem de erro tarefa 0 não encontrada


  @VerificaTarefaCampoPesquisaSimboloNoCampoPesquisa
  Cenario: Valida mensagem de erro ao inserir um simbolo no campo Pesquisa
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E insiro um símbolo no campo de pesquisa
    Entao valido mensagem de erro simbolo não esperado


  @TarefaAdicionaMarcadorComSucesso
  Cenario: Valida adição de marcador em tarefa com sucesso
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E preencho o campo de Marcadores com um marcador
    E clico em Aplicar Marcador
    Entao verifico no banco de dados se o marcador existe


  @TarefaExcluirMarcadorComSucesso
  Cenario: Valida exclusão de marcador em tarefa com sucesso
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E preencho o campo de Marcadores com um marcador
    E clico em Aplicar Marcador
    E verifico no banco de dados se o marcador existe
    E Excluo o marcador criado
    Entao verifico no banco de dados se o marcador foi excluido com sucesso


  @TarefaAdicionaRelacaoSucesso
  Esquema do Cenario: Valida adição de Relação em tarefa com sucesso
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com o ID da tarefa a ser relacionada
    E clico em Adicionar
    Entao verifico no banco de dados se foi adicionada a relação do tipo 1 a tarefa
    Exemplos:
      | TIPORELACAO        |
      | está relacionado a |

  @TarefaAdicionaRelacaoPaiSucesso
  Esquema do Cenario: Valida adição de Relação do tipo Pai em tarefa com sucesso
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com o ID da tarefa a ser relacionada
    E clico em Adicionar
    Entao verifico no banco de dados se foi adicionada a relação do tipo 2 Pai a tarefa
    Exemplos:
      | TIPORELACAO |
      | é pai de    |


  @TarefaAdicionaRelacaoFilhoSucesso
  Esquema do Cenario: Valida adição de Relação do tipo Filho em tarefa com sucesso
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com o ID da tarefa a ser relacionada
    E clico em Adicionar
    Entao verifico no banco de dados se foi adicionada a relação do tipo 2 Filho a tarefa
    Exemplos:
      | TIPORELACAO |
      | é filho de  |


  @TarefaAdicionaRelacaoDuplicadaSucesso
  Esquema do Cenario: Valida adição de Relação do tipo Duplicada em tarefa com sucesso
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com o ID da tarefa a ser relacionada
    E clico em Adicionar
    Entao verifico no banco de dados se foi adicionada a relação do tipo 0 a tarefa
    Exemplos:
      | TIPORELACAO    |
      | é duplicado de |


  @TarefaAdicionaRelacaoPossuiDuplicadaSucesso
  Esquema do Cenario: Valida adição de Relação que possui duplicada em tarefa com sucesso
    Dado que tenho uma tarefa registrada
    E crio uma tarefa Duplicada
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com o ID da tarefa a ser relacionada
    E clico em Adicionar
    Entao verifico no banco de dados se foi adicionada a relação do tipo 0 Possui Duplicado a tarefa
    Exemplos:
      | TIPORELACAO      |
      | possui duplicado |

  @ValidaErroAoAdicionarRelacaoInvalida
  Esquema do Cenario: Valida erro Relacao Invalida - ID Não encontrado
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com um ID que é inexistente
    E clico em Adicionar
    Entao valido mensagem de erro id da tarefa nao encontrada
    Exemplos:
      | TIPORELACAO        |
      | está relacionado a |

  @ValidaErroAoAdicionarRelacaoVazia
  Esquema do Cenario: Valida erro Relacao Invalida - Campo usuario Vazio
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E deixo o campo de usuario vazio
    E clico em Adicionar
    Entao valido mensagem de erro campo usuario vazio
    Exemplos:
      | TIPORELACAO        |
      | está relacionado a |


  @ValidaErroAoAdicionarRelacaoInvalidaLetrasNoCampo
  Esquema do Cenario: Valida erro Relacao Invalida - Valor Invalido Letras
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com letras
    E clico em Adicionar
    Entao valido mensagem de erro valor invalido
    Exemplos:
      | TIPORELACAO        |
      | está relacionado a |


  @ValidaErroAoAdicionarRelacaoInvalidaHifenESimbolosNoCampo
  Esquema do Cenario: Valida erro Relacao Invalida - Valor Invalido Hifen e simbolos
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com hifens e simbolos
    E clico em Adicionar
    Entao valido mensagem de erro valor invalido hifens e simbolos
    Exemplos:
      | TIPORELACAO        |
      | está relacionado a |


  @ValidaErroAoAdicionarRelacaoIDPropriaTarefa
  Esquema do Cenario: Valida erro Relacao Invalida - Não pode se relacionar consigo mesmo
    Dado que tenho uma tarefa registrada
    E crio uma segunda tarefa
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E seleciono relacao "<TIPORELACAO>"
    E preencho campo de relacao com o ID da tarefa da pagina atual
    E clico em Adicionar
    Entao valido mensagem de erro tarefa não pode relacionar consigo mesmo
    Exemplos:
      | TIPORELACAO        |
      | está relacionado a |


  @ValidaAdiçãoDeUsuarioEmMonitoramentoDeTarefaSucesso
  Cenario: Valida adição de um usuário em um monitoramento de uma tarefa com sucesso
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E preencho campo usuários monitorando tarefa  o nome do usuario desejado
    E clico em Adicionar Usuario
    Entao valido no banco que o usuario agora esta monitorando a tarefa


  @ValidaExclusãoDeUsuarioEmMonitoramentoDeTarefaSucesso
  Cenario: Valida exclusão de um usuário em um monitoramento de uma tarefa com sucesso
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E preencho campo usuários monitorando tarefa  o nome do usuario desejado
    E clico em Adicionar Usuario
    E excluo o usuario adicionado
    Entao valido no banco que o usuario não esta mais na tabela


  @ValidaErroDeUsuarioEmMonitoramentoDeTarefaUsuarioNaoEncontrado
  Cenario: Valida mensagem erro inserindo usuario invalido
    Dado que tenho uma tarefa registrada
    Quando acesso a tela Ver Tarefa
    E pesquiso pelo ID da tarefa
    E preencho campo usuários monitorando tarefa  o nome de um usuario invalido
    E clico em Adicionar Usuario
    Entao valido mensagem de erro usuario não encontrado







