# DesafioWebBase2Selenium - SERENITY BDD

### Projeto criado para o DesafioWeb utilizando o framework SerenityBDD

# Fazendo o Set-up do Projeto localmente

### O projeto utiliza os seguintes itens 
    
- Apache Maven - ver. 3.8.6
- Framework SerenityBDD - ver. 3.2.5 (MAVEN) - Implementado com Cucumber + JUnit 4
- Imagem Docker MantisBT + Selenium Grid (1 Hub e 3 Nodes)
- Ferramenta de Report Interna - SerenityBDD Reports
- Vizualizador/Editor de BD - HeidiSQL


# PASSOS PARA A INSTALAÇÃO DOS COMPONENTES

## 1. Configurando o Projeto
### - Verifique se possui o IntelliJ ou qualquer outra IDE compativel com Maven instalado

### - Faça o clone deste projeto em sua maquina 

### - No canto esquerdo da IDE terá toda a estrutura do projeto

### - procure pela pasta `localsetupFiles`


![image](/uploads/ef2e6530436919d5eff62ec83c247630/image.png)

### pegue o arquivo `.zip` e extraia a pasta para uma localização de fácil acesso como: `C:\Program Files`

![image](/uploads/36a93089b811c8984dc9dddd0b419922/image.png)


![image](/uploads/efefef983c3f47e467e9180e54425601/image.png)

### Após fazer isso abra a pasta extraida até a chegar na pasta `\bin`

![image](/uploads/b13b5fd994802be1b61ac4430ea6586c/image.png)

### Copie o caminho todo que contem até a pasta `\bin`  Exemplo: `C:\Program Files\apache-maven-3.8.6\bin`

![image](/uploads/8d19def8fecb4877d798f6d07b1772e5/image.png)

### Cole esse caminho no Path acessando as configurações de `Variáveis de Ambiente` do windows e depois clicando no Path de `Variáveis do Sistema`


![image](/uploads/9a3fbe9479cbc2d83a6c03a6106f1c6f/image.png)


### Aplique a configuração e reinicie a IDE

### No terminal da IDE rode o comando `mvn -v` para verificar se o maven foi instalado com sucesso

![image](/uploads/02ae9a18d58812e541c61fadfd08e4ec/image.png)

>Resultado do comando executado acima

![image](/uploads/efee28e7a9526a818084338fc1c519f3/image.png)

### Após isso faça a primeira execução para instalar todas as dependecias necessarias no ambiente 

### Para isso basta rodar `mvn clean install -DskipTests=true`

![image](/uploads/7db8c579eafeb25e3f1af1348cf2451b/image.png)

>Resultado do comando executado acima

![image](/uploads/3e2a5b159c331cd124a77633cf2b1827/image.png)


## 2. Configurando o Docker

### 1.  Instalar [Docker Desktop](https://www.docker.com/products/docker-desktop) e reiniciar a máquina
### 2.  Caso apresente o erro "WSL 2 installation is incomplete", [baixe e instale o WSL2 Kernel](https://docs.microsoft.com/pt-br/windows/wsl/wsl2-kernel) e clique em Restart

![](https://i.imgur.com/4wHESjW.png)

### 3.  Abra o aplicativo Docker Desktop

![](https://i.imgur.com/cyAeSa2.png)

### 4.  Deverá ser apresentado o tutorial, basta dar skip que você terá esta tela

![](https://i.imgur.com/Myxqwmv.png)

### 5. Assim que o aplicativo do Docker foi configurado, abra a pasta `localsetupFiles` que esta dentro da estrutura do projeto

![image](/uploads/ff155b1151d28ed4603df8114cf80372/image.png)

### 5.1 pegue essa pasta da imagem acima e coloque ela num diretório de facil acesso Exemplo `C:\mantis\`

### 5.2 No diretório criado haverá o arquivo **docker-compose.yml**

### 6.  Abra o terminal nessa pasta com o arquivo e execute o comando> `docker-compose.exe up -d`

### 7.  Após o processamento se tudo correr bem, as imagens serão baixadas e novos contêineres criados:

![](https://i.imgur.com/TPbVjVQ.png)

### 8.  Para validar a criação e execução dos execute o comando `docker ps -a` e os contêineres estarão disponíveis e executando:

![](https://i.imgur.com/4pZ3IEQ.png)

### 9. No aplicativo do Docker Desktop apresentará os containeres ativos conforme imagem:
    
### Explicação de cada container

- **mantis_db_1 [Banco de Dados] -> porta padrão: 3306**
- **mantis_selenium-hub_1 [Selenium Grid Hub] -> portas padrão: 4442:4443:4444**
- **mantis_mantisbt_1 [Site MantisBT] -> porta padrão: 8989**
- **mantis_chrome_1 [Selenium Grid Node (Chrome)]** 
- **mantis_edge_1 [Selenium Grid Node (MSEdge)]** 
- **mantis_firefox_1 [Selenium Grid Node (Firefox)]** 

![image](/uploads/536587eeef3634a00be958aafd96a314/image.png)



# Configuração padrão Site MantisBT

### O site para funcionar corretamente com a automação ele deve ser configurado com os seguintes parâmetros

### Faça o seu primeiro acesso ao Mantis pelo endereço http://127.0.0.1:8989

### Após acessar será necessário configurar o banco de dados conforme tabela e valores abaixo:

| Variável | Valor          |
|-----|----------------|
| Type of Database | MySQL Improved |
| Hostname (for Database Server) | mantis-db-1    |
| Username (for Database) | mantisbt       |
| Password (for Database) | mantisbt       |
| Database name (for Database) | bugtracker     |
| Admin Username (to create Database if required) | root           |
| Admin Password (to create Database if required) | root           |


### IMPORTANTE: O Banco de dados gera automaticamente um usuario administrador por conta da massa scriptada do projeto

#### Valores padrões para login caso necessite teste manual: `Usuário: administrator` `Senha: mantispassbt`

## 6. Entendendo configurações importantes do Serenity.properties

### As configurações atuais desse projeto são voltadas para as execuções no CI/CD e no Selenium Grid

### Porém é muito pratico fazer quaisquer alterações necessarias

### Basta acessar o arquivo serenity.properties localizado na estrutura do projeto

![image](/uploads/36f76fbd827d97dd649f9926a172c606/image.png)

### Logo abaixo está o padrão de como deve ser executado apenas na maquina local os testes

### Sempre utilize `#` para comentar as propriedades que não irá utilizar, e caso queira utilizalas só remover o `#`

```
serenity.project.name=DESAFIO WEB SELENIUM
#FOR_EACH_ACTION / BEFORE_AND_AFTER_EACH_STEP / AFTER_EACH_STEP / FOR_FAILURES / DISABLED
serenity.take.screenshots=AFTER_EACH_STEP
#serenity.step.delay=250
#serenity.verbose.steps=false
#serenity.driver.capabilities=acceptInsecureCerts:true;browserName:chrome;browserVersion:103.0;platformName:LINUX;se:noVncPort:7900;se:vncEnabled:true
#NORMAL,QUIET,VERBOSE
#serenity.logging=VERBOSE
#serenity.browser.maximize=true
serenity.browser.width=1366
serenity.browser.height=728
#SWITCHES
chrome.switches=--lang=pt_BR
#ip-DOCKER- UTILIZE O IP DO SEU DOCKER ENCONTRADO NO CMD NA PROPRIEDADE 'webdriver.base.url=http://IPDOCKER:8989/' 
webdriver.base.url=http://127.0.0.1:8989/ 
webdriver.timeouts.implicitlywait=10000
webdriver.wait.for.timeout=10000
#firefox chrome iexplorer phantomjs htmlunit
webdriver.driver=chrome
webdriver.chrome.driver=chromedriver.exe
webdriver.gecko.driver=geckodriver.exe
webdriver.edge.driver=msedgedriver.exe
#webdriver.remote.url=http://localhost:4444
#webdriver.remote.driver=chrome
#webdriver.remote.os=LINUX
chrome_preferences.download.prompt_for_download=false
```





## 4. Executando os casos com sucesso


### A partir daqui se tudo foi configurado de acordo, apenas acesse o terminal da IDE e execute `mvn clean verify`
#### _Caso queira um cenário em especifico execute `mvn clean verify -Dtags=@TagQueDesejaExecutar` desse jeito você conseguirá executar um unico cenário ou um test suite com report integrado a partir das tags_
![image](/uploads/40508e511ed57be019641176e9a8b3f8/image.png)


### Assim que todos os casos terminarem de executar ele retorna uma mensagem de BUILD SUCCESS e logo acima um link direto para acessar o relatorio gerado pelo próprio framework

![image](/uploads/c1e8ef0a4a4feb672506953d131c39c2/image.png)



## 5. Verificando o caminho do Report

### O Report além de ser gerado por um link direto como é mostrado na etapa anterior, o acesso manual dele caso queira verificar manualmente é a pasta `target/site/`

![image](/uploads/e7eec8654078f1e6013f174bc58df8b2/image.png)

### Dentro dessa pasta basta apenas procurar pelo arquivo `index.html` que automaticamente assim que abrir o report já vem organizado

![image](/uploads/aad28f232c783d5178d8d4e5f3910b3b/image.png)

![image](/uploads/c0d64640deda1d19d8e4f4bcc1938802/image.png)

## Dicas do Report

### Caso queira fazer um report de um teste totalmente novo digite no terminal `mvn clean` para apagar a pasta target e depois `mvn clean verify` para executar o que quiser

### Caso queira acrenscentar um novo teste a um report já criado ou até mesmo validar novamente um caso de teste em especifico basta rodar o caso de teste através do runner do Maven na própria feature

![image](/uploads/a7fa34d0990638370dd5e88ae9fee1dc/image.png)

### E depois execute `mvn serenity:aggregate` no terminal para acrescentar o teste executado ao report já existente

## 6. Executando os testes através do Selenium Grid

### Como havia sido mostrado antes, o controle de toda a execução esta no serenity.properties

### Então caso você tenha alterado para a configuração de execução local 

### basta alterar na parte final do arquivo os seguintes elementos:

```
#ip-DOCKER- UTILIZE O IP DO SEU DOCKER ENCONTRADO NO CMD NA PROPRIEDADE 'webdriver.base.url=http://IPDOSEUDOCKER:8989/' 
webdriver.base.url=http://IPDOSEUDOCKER:8989/ 
webdriver.timeouts.implicitlywait=10000
webdriver.wait.for.timeout=10000
#firefox chrome iexplorer phantomjs htmlunit
webdriver.driver=chrome
webdriver.chrome.driver=chromedriver.exe
webdriver.gecko.driver=geckodriver.exe
webdriver.edge.driver=msedgedriver.exe
webdriver.remote.url=http://localhost:4444
webdriver.remote.driver=chrome
webdriver.remote.os=LINUX
```

### O que foi feito logo acima foi apenas a alteração do ip do `webdriver.base.url` de localhost (ou 127.0.0.1) para o IP do Docker da sua maquina

### também removido os `#` do `webdriver.remote.url` , `webdriver.remote.driver` e `webdriver.remote.os=LINUX`

### Após isso execute normalmente no terminal que automaticamente ira ativar o Node no Grid localizado no ip http://localhost:4444

### Pagina do Selenium Grid ativa com os Nodes

![image](/uploads/6a92d56d145453f9670cae85ff353468/image.png)



## 7. Configuração projeto para CI/CD

### Dentro do arquivo `.gitlab-ci.yml` possui toda a configuração para executar a job no gitlab

### O final da job valida os testes e produz um relatório SerenityBDD Reports para download por 1 semana 

![image](/uploads/95e8947d9b6fc15ce53d1e35121ecd55/image.png)


## REFERÊNCIAS

### Serenity BDD Learn Page - https://serenity-bdd.github.io/theserenitybook/latest/cucumber.html

### Serenity with Selenium - https://www.lambdatest.com/support/docs/serenity-test-on-selenium-grid/

### Docker - selenium - https://github.com/SeleniumHQ/docker-selenium

### Mantis - Docker - https://github.com/saymowan/Mantis4Testers-Docker


